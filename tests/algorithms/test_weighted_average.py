import numpy
from analysta.algorithms.weighted_average import WeightedAverage


def test_single_weight_single_channel():
    model = WeightedAverage([1])
    real = numpy.random.random((1024, 1))
    pred = model.predict(real)

    assert pred.shape == real.shape
    assert pred[-1] == real[-1]


def test_two_weights_single_channel():
    model = WeightedAverage([1, 1])
    real = numpy.random.random((1024, 1))
    pred = model.predict(real)

    assert pred.shape == (real.shape[0] - 1, real.shape[1])
    assert pred[-1] == (real[-1] + real[-2]) / 2.0


def test_two_different_weights_single_channel():
    model = WeightedAverage([3, 2])
    real = numpy.random.random((1024, 1))
    pred = model.predict(real)

    assert pred.shape == (real.shape[0] - 1, real.shape[1])
    assert pred[-1] == (3 * real[-1] + 2 * real[-2]) / 5.0
