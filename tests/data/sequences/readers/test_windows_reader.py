import numpy as np
from analysta.data.sequences.readers.windows_reader import WindowsReader


def test_creation_look_back_is_equal_current():
    data = np.random.random((3, 129, 3))

    reader = WindowsReader(name='default', data=[data], in_channels=[0, 1], out_channels=[2], labels_channel=2,
                           look_back=128, batch_size=2, samples_percentage=1, return_indexes=False,
                           shuffle=False, chunk_size=1)

    batch_x, batch_y, _ = reader.calc_item(0)
    assert np.array_equal(batch_x, data[0:2, 0:128, 0:2])
    assert np.array_equal(batch_y, data[0:2, 128, [2]])

    batch_x, batch_y, _ = reader.calc_item(1)
    assert np.array_equal(batch_x, data[2:3, 0:128, 0:2])
    assert np.array_equal(batch_y, data[2:3, 128, [2]])


def test_creation_look_back_is_greater_than_current():
    data = np.random.random((3, 129, 3))

    reader = WindowsReader(name='default', data=[data], in_channels=[0, 1], out_channels=[2], labels_channel=2,
                           look_back=64, batch_size=16, samples_percentage=1, return_indexes=False,
                           shuffle=False, chunk_size=1)

    batch_x, batch_y, _ = reader.calc_item(0)
    assert np.array_equal(batch_x[0], data[0, 0:64, 0:2])
    assert np.array_equal(batch_y[0], data[0, 64, [2]])
    assert np.array_equal(batch_x[15], data[0, 15:79, 0:2])
    assert np.array_equal(batch_y[15], data[0, 79, [2]])

    batch_x, batch_y, _ = reader.calc_item(1)
    assert np.array_equal(batch_x[0], data[0, 16:80, 0:2])
    assert np.array_equal(batch_y[0], data[0, 80, [2]])

    batch_x, batch_y, _ = reader.calc_item(4)
    assert np.array_equal(batch_x[0], data[0, 64:128, 0:2])
    assert np.array_equal(batch_y[0], data[0, 128, [2]])


def test_chunk_size_greater_than_one():
    data = np.random.random((3, 129, 3))

    reader = WindowsReader(name='default', data=[data], in_channels=[0, 1], out_channels=[2], labels_channel=2,
                           look_back=64, batch_size=16, samples_percentage=1, return_indexes=False,
                           shuffle=False, chunk_size=8)

    batch_x, batch_y, _ = reader.calc_item(0)
    assert np.array_equal(batch_x[0], data[0, 0:64, 0:2])
    assert np.array_equal(batch_y[0], data[0, 64, [2]])
    assert np.array_equal(batch_x[1], data[0, 1:65, 0:2])
    assert np.array_equal(batch_y[1], data[0, 65, [2]])

    batch_x, batch_y, _ = reader.calc_item(1)
    assert np.array_equal(batch_x[0], data[0, 16:80, 0:2])
    assert np.array_equal(batch_y[0], data[0, 80, [2]])

    # start of chunk needs to align with start of batch
    batch_x, batch_y, _ = reader.calc_item(4)
    assert np.array_equal(batch_x[0], data[1, 0:64, 0:2])
    assert np.array_equal(batch_y[0], data[1, 64, [2]])
