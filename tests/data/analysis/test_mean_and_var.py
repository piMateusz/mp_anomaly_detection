import numpy

from analysta.data.analysis.mean_and_var import *


def test_mean_and_var_calculation_for_batch():
    channels = 4
    data = numpy.ones((1, 1024, 256, channels))

    batches_means = numpy.empty((1, channels))
    batches_vars = numpy.empty((1, channels))

    for i in range(channels):
        data[:, :, :, i] *= i+1

    batches_means[0], batches_vars[0] = get_mean_and_variance_for_batch(data[0])

    assert numpy.array_equal(batches_means, [range(1, channels+1)])
    assert numpy.array_equal(batches_vars, [[0]*channels])


def test_mean_and_var_calculation_for_dataset():
    batches = 3

    data = numpy.random.randint(-2**19, 2**19, (batches, 1024, 256, 4))

    real_means = numpy.mean(data, axis=(0, 1, 2))
    real_vars = numpy.var(data, axis=(0, 1, 2))

    batches_means = numpy.empty((batches, 4))
    batches_vars = numpy.empty((batches, 4))

    for i, batch in enumerate(data):
        batches_means[i], batches_vars[i] = get_mean_and_variance_for_batch(batch)

    comp_means, comp_vars = get_mean_and_variance_for_dataset(batches_means, batches_vars, [1024] * batches)

    assert numpy.allclose(real_means, comp_means)
    assert numpy.allclose(real_vars, comp_vars)
