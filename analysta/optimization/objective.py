import logging

from analysta.cli.model.run_single import run_single
from analysta.metrics import HUMANIZE_METRICS
from analysta.optimization.space_utils import update_config_with_space_kwargs
from analysta.utils.config import Config


def objective(config_path, writer_wrapper, results_dir, metric, reverse_metric=False, **kwargs):
    logger = logging.getLogger(__name__)
    logger.info('Testing:\n\t{}'.format('\n\t'.join([
        '{}={}'.format(key, value)
        for key, value in kwargs.items()
    ])))

    config = Config(config_path)
    update_config_with_space_kwargs(config, kwargs)

    config, setup_writer, detector = run_single(config, setup_writer=writer_wrapper[0], results_dir=results_dir)
    writer_wrapper[0] = setup_writer

    result = detector.stats[metric]
    metric_key = metric.split('.')[-1]
    logger.info('{}: {}'.format(
        HUMANIZE_METRICS[metric_key]['long'] if metric_key in HUMANIZE_METRICS else metric_key.upper(),
        result
    ))

    if reverse_metric:
        result = 1 - result

    return result
