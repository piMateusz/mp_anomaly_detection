from sklearn.neighbors import KNeighborsClassifier

from analysta.data.sequences import FlatDataSequence
from analysta.models.interfaces.model import Model


class KNNModel(Model):
    data_sequence_class = FlatDataSequence

    def __init__(self, *args, **kwargs):
        super(KNNModel, self).__init__(*args, **kwargs)

        self.__n_neighbors = self._config.setdefault('model.n_neighbors', 5)
        self.__weights = self._config.setdefault('model.weights', 'uniform')
        self.__algorithm = self._config.setdefault('model.algorithm', 'auto')
        self.__leaf_size = self._config.setdefault('model.leaf_size', 30)
        self.__metric = self._config.setdefault('model.metric', 'minkowski')
        self.__metric_p = self._config.setdefault('model.p', 2)

    def _create_model(self):
        return KNeighborsClassifier(n_neighbors=self.__n_neighbors, weights=self.__weights, algorithm=self.__algorithm,
                                    leaf_size=self.__leaf_size, p=self.__metric_p, metric=self.__metric, n_jobs=-1)
