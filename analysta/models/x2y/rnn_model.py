from analysta.models.interfaces.nn_model import NNModel
from analysta.utils import get_definition_from_path


class RNNModel(NNModel):
    def __init__(self, *args, **kwargs):
        super(RNNModel, self).__init__(*args, **kwargs)

        self._cells = self._config['model.cells']  # no default on purpose
        self._cells = [int(c) for c in self._cells if c > 0]  # ensure we have Python ints and skip zeros

    def _create_template_model(self):
        from keras import Sequential
        from keras.layers import Dense

        model = Sequential()
        for i, cells in enumerate(self._cells):
            kwargs = {
                'return_sequences': i != len(self._cells) - 1
            }
            if i == 0:
                kwargs['input_shape'] = (self._x_shape[-2], self._x_shape[-1])
            model.add(self.rnn_class(cells, **kwargs))

        # TODO: correctly handle y_shape
        if self._is_classifier:  # for one hot
            model.add(Dense(self._y_shape[-1], activation='softmax'))
        else:
            model.add(Dense(self._y_shape[-1], activation='linear'))

        return model

    @property
    def rnn_class(self):
        """
        RNN-type Layer to be use in model. By default keras.layers.LSTM
        :return:
        """
        return get_definition_from_path(self._config.setdefault('model.rnn_class', 'keras.layers.LSTM'), 'RNNClass')
