import numpy
from sklearn.metrics import mean_squared_error

from analysta.data.sequences.x2x_data_sequence import X2XDataSequence
from analysta.models.interfaces.model import Model
from analysta.models.dummy_history import DummyHistory


class X2XModel(Model):
    data_sequence_class = X2XDataSequence

    def __init__(self, *args, **kwargs):
        super(X2XModel, self).__init__(*args, **kwargs)
        self._is_classifier = False

    def fit(self, train_generator, val_generator):
        force_eval = train_generator.dataset_mean is None or train_generator.dataset_var is None
        self._set_dataset_mean_and_var(train_generator, val_generator, force_eval=force_eval)

        val_mse = []
        for y_val, y_pred in self.predict(val_generator, limit=len(val_generator)):
            val_mse.append(mean_squared_error(
                y_val,
                y_pred
            ))

        return DummyHistory(val_mse=val_mse)

    def _predict_on_batch(self, x):
        predictions = []
        for sample in x:
            predictions.append(self.model.predict(sample)[-len(sample):])
        return numpy.array(predictions)

    @property
    def uses_single_out_channel(self):
        return False
