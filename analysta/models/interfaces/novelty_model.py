import numpy
from analysta.models.interfaces.outlier_model import OutlierModel


class NoveltyModel(OutlierModel):
    def _get_concatenated_train_set(self, train_generator):
        x_train, y_train = super(NoveltyModel, self)._get_concatenated_train_set(train_generator)

        # hack that will allow to test other detectors
        # makes sense only if we have labeled dataset and out_bucketization="none"
        # will filter out anything other than 1 ("normal" category)
        x_train = numpy.compress(y_train.reshape((-1,)) == 1, x_train, axis=0)
        y_train = numpy.ones((x_train.shape[0], ))

        return x_train, y_train
