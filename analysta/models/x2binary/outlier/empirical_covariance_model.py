from analysta.models.interfaces.outlier_model import OutlierModel
from sklearn.covariance import EllipticEnvelope


class EmpiricalCovarianceModel(OutlierModel):
    def _create_model(self):
        return EllipticEnvelope(contamination=self._contamination, support_fraction=1.0)
