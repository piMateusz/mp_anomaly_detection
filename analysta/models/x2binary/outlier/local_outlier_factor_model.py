from analysta.models.interfaces.outlier_model import OutlierModel
from sklearn.neighbors import LocalOutlierFactor


class LocalOutlierFactorModel(OutlierModel):
    def __init__(self, *args, **kwargs):
        super(LocalOutlierFactorModel, self).__init__(*args, **kwargs)
        self.__neighbors = self._config.setdefault('model.neighbors', 35)

    def _create_model(self):
        return LocalOutlierFactor(n_neighbors=self.__neighbors, contamination=self._contamination)
