from sklearn import svm
from analysta.models.interfaces.novelty_model import NoveltyModel


class OneClassSVMModel(NoveltyModel):
    def __init__(self, *args, **kwargs):
        super(OneClassSVMModel, self).__init__(*args, **kwargs)

        self.__kernel = self._config.setdefault('model.kernel', 'rbf')
        self.__nu = None  # needs contamination to be known
        self.__gamma = self._config.setdefault('model.gamma', 0.1)

    def _create_model(self):
        return svm.OneClassSVM(nu=self.nu, kernel=self.__kernel, gamma=self.__gamma, verbose=True)

    @property
    def nu(self):
        if self.__nu is None:
            self.__nu = self._config.setdefault('model.nu', 0.95 * self._contamination + 0.05)
        return self.__nu
