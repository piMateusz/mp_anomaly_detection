import numpy
from sklearn.dummy import DummyClassifier
from analysta.models.interfaces.model import Model


class RandomModel(Model):
    def __init__(self, *args, **kwargs):
        super(RandomModel, self).__init__(*args, **kwargs)

        self.__strategy = self._config.setdefault('model.strategy', 'stratified')
        self.__constant = None

        if isinstance(self.__strategy, (int, list)):
            self.__constant = self.__strategy
            self.__strategy = 'constant'

    def _create_model(self):
        return DummyClassifier(strategy=self.__strategy, constant=self.__constant)

    def _get_concatenated_train_set(self, train_generator):
        if self.__strategy != 'constant':
            x_train, y_train = super(RandomModel, self)._get_concatenated_train_set(train_generator)
        else:
            # a bit hack-ish, but now we can use DataSequence's one_hot_encoder
            if self.is_classifier:
                self.__constant = train_generator.to_one_hot(numpy.array([self.__constant]).reshape((1, -1)))[0]
                self.model.set_params(constant=self.__constant)
            x_train, y_train = super(RandomModel, self)._get_concatenated_train_set([train_generator[0]])

        return x_train, numpy.squeeze(y_train)
