import logging

import numpy

from analysta.algorithms.double_exponential_smoothing import DoubleExponentialSmoothing
from analysta.models.interfaces.univariate_model import UnivariateModel


class DoubleExponentialSmoothingModel(UnivariateModel):
    def __init__(self, config, x_shape, y_shape, model=None):
        super(DoubleExponentialSmoothingModel, self).__init__(config, x_shape, y_shape, model)

        self.__alpha = numpy.array(config.setdefault('model.alpha', [0.9] * x_shape[1]))
        self.__beta = numpy.array(config.setdefault('model.beta', [0.9] * x_shape[1]))

        self._verify_param_compatibility(self.__alpha, 'model.alpha')
        self._verify_param_compatibility(self.__beta, 'model.beta')

    def _create_model_for_channel(self, channel_index):
        return DoubleExponentialSmoothing(
            self.__alpha[channel_index],
            self.__beta[channel_index]
        )
