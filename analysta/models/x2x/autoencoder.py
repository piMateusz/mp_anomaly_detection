from analysta.data.sequences.x2x_data_sequence import X2XDataSequence
from analysta.models.x2y import RNNModel


class AutoencoderModel(RNNModel):
    """
    Created based on:
    https://stackoverflow.com/questions/49945857/lstm-autoencoder-for-time-series-prediction
    """
    def __init__(self, config, *args, **kwargs):
        # set other defaults than in NNModel
        self._model_optimizer = config.setdefault('model.optimizer', 'rmsprop')
        self._model_loss = config.setdefault('model.loss', 'mean_squared_error')
        self._model_metrics = config.setdefault('model.metrics', ['cosine_proximity'])

        super(AutoencoderModel, self).__init__(config, *args, **kwargs)
        self._data_sequence_class = X2XDataSequence
        self._is_classifier = False

    def _create_template_model(self):
        from keras import Sequential
        from keras.layers import RepeatVector

        model = Sequential()

        # encoder
        for i, cells in enumerate(self._cells):
            kwargs = {
                'return_sequences': i != len(self._cells) - 1,
                'name': 'encoder_{}'.format(i)
            }
            if i == 0:
                kwargs['input_shape'] = (self._x_shape[-2], self._x_shape[-1])
            model.add(self.rnn_class(cells, **kwargs))

        model.add(RepeatVector(self._x_shape[-2]))

        # decoder
        decoder_cells = self._cells[:-1]
        decoder_cells.reverse()
        decoder_cells.append(self._x_shape[-1])
        for i, cells in enumerate(decoder_cells):
            kwargs = {
                'return_sequences': True,
                'name': 'decoder_{}'.format(i)
            }
            model.add(self.rnn_class(cells, **kwargs))

        return model

# anomaly detection should be done based on
# https://arxiv.org/pdf/1607.00148.pdf - point 2.2
# maximum likelihood estimation is used in the original paper:
# https://towardsdatascience.com/probability-concepts-explained-maximum-likelihood-estimation-c7b4342fdbb1
# Bayesian approach may be considered as an extension ? (option)
# https://towardsdatascience.com/probability-concepts-explained-bayesian-inference-for-parameter-estimation-90e8930e5348
