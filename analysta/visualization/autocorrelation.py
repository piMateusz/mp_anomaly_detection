import os
import sys
import humanize
import numpy as np
from tqdm import tqdm


def plot_batch_autocorrelation(batch_x, idx, analyzed_channels, channels_names, indexes, results_dir,
                               filename='autocorr_batch-{}.pdf'):
    from matplotlib import pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages

    with PdfPages(os.path.join(results_dir, filename.format(idx))) as pdf:
        with tqdm(total=batch_x.shape[0], file=sys.stdout) as pbar:
            for window, (series, index_in_series) in zip(batch_x, indexes):
                fig = plt.figure()
                fig.suptitle('series {}, samples {} - {}'.format(
                    series,
                    humanize.intcomma(index_in_series),
                    humanize.intcomma(index_in_series + len(window))
                ))
                axes = fig.subplots(window.shape[1], sharex=True)
                for i, ch in enumerate(analyzed_channels):
                    x = window[:, i]
                    autocorrelation = np.correlate(x, x, mode='same')
                    plot_start = int(autocorrelation.size / 2)
                    axes[i].bar(np.arange(len(autocorrelation) - plot_start), autocorrelation[plot_start:])
                    axes[i].set_title('{}'.format(channels_names[ch]))
                pdf.savefig(fig)
                plt.close(fig)
                pbar.update(1)
