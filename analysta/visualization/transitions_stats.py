import argparse
import csv
import os
import numpy
import pprint

import sys

from analysta.utils.config import Config
from analysta.cli.arguments.types import readable_file
from analysta.data.utils import convert_to_categories, split_predictions_into_series

COUNT_CARDINALITY = False
COUNT_TRANSITIONS = False and COUNT_CARDINALITY
SHOW_ZERO_TRANSITIONS = False and COUNT_TRANSITIONS

SHOW_PRED = False

PRINT_CARDINALITY = True and COUNT_CARDINALITY
PRINT_INDIVIDUAL = False


def run(dataset, config_path=None, edges_path=None, setup_path=None):
    if SHOW_PRED:
        detector_results_path, look_ahead, look_back, edges_path = load_info(setup_path)
    else:
        detector_results_path = None
        look_ahead = 1
        look_back = 1

    edges = numpy.load(edges_path)
    config = Config(config_path)
    paths = config.load_paths('data.' + dataset + '_paths')

    real_data_dict = prepare_real_data_dict(edges['out_edges'], look_ahead, look_back, config, dataset)
    real_results, cardinality_stats = count_stats(real_data_dict, edges['out_edges'])
    print_stats(real_results, cardinality_stats, 'real', config, dataset)

    sizes = numpy.array([real_results[d]['length'] - look_ahead - look_back
                         for d in paths])

    print(edges['out_edges'])

    if len(edges['out_edges']) > 1:
        anomalies = numpy.array([numpy.count_nonzero(v.argmax(axis=1)) for v in real_data_dict.values()])

        print("series with anomalies:")
        pprint.pprint(dict(zip(*numpy.unique(anomalies, return_counts=True))))

        print("series with n length counts:")
        pprint.pprint(dict(zip(*numpy.unique(sizes + look_ahead + look_back, return_counts=True))))

    del real_results

    if SHOW_PRED:
        pred_data_dict = prepare_pred_data_dict(detector_results_path, sizes, look_ahead, look_back, dataset)
        pred_results = count_stats(pred_data_dict, edges['out_edges'])
        print_stats(pred_results, None, 'predicted', config, dataset)


def load_info(setup_path):
    results_dir = os.path.dirname(setup_path)

    with open(setup_path, 'rb') as setup_file:
        reader = csv.DictReader(setup_file)
        for row in reader:
            detector_results_path = os.path.join(results_dir, os.path.basename(row['detector_results']))
            look_ahead = int(row['look_ahead'])
            look_back = int(row['look_back'])
            edges_path = row['edges']

            return detector_results_path, look_ahead, look_back, edges_path


def prepare_pred_data_dict(detector_results_path, sizes, look_ahead, look_back, dataset):
    data = numpy.load(detector_results_path)
    predicted = data[dataset + '_predicted']
    predicted_by_series = split_predictions_into_series(predicted, sizes, look_back + look_ahead)
    return predicted_by_series


def prepare_real_data_dict(out_edges, look_ahead, look_back, config, dataset):
    real_data_dict = {}
    data_paths = config.load_paths('data.' + dataset + '_paths')
    output_indexes = config['data.output_channels']

    for name, path in data_paths:
        data = numpy.load(path)

        real_categories = convert_to_categories(
            data['data'][look_ahead + look_back:, output_indexes],
            out_edges,
            len(out_edges) + 1
        )

        real_data_dict[name] = real_categories
    return real_data_dict


def count_stats(data_dict, edges):
    nb_classes = len(edges) + 1
    real_results = {}

    for dd in data_dict.keys():
        real_categories = data_dict[dd]

        real_numerical_categories = numpy.argmax(real_categories, axis=1)

        real_results[dd] = {
            'length': real_numerical_categories.shape[0]
        }

        if COUNT_CARDINALITY:
            real_hist, bin_edges = numpy.histogram(
                real_numerical_categories,
                bins=nb_classes,
                range=(0, nb_classes)
            )
            real_nonzero = numpy.nonzero(real_hist)[0]
            real_results[dd]['cardinality'] = dict(zip(real_nonzero, real_hist[real_nonzero]))

            if COUNT_TRANSITIONS:
                real_transitions = count_transitions(real_nonzero, real_numerical_categories)
                real_results[dd]['transitions'] = real_transitions

    if COUNT_CARDINALITY:
        cardinality_stats = {
            'sum': [0] * nb_classes,
            'classes': nb_classes
        }

        for dd in data_dict.keys():
            for k in range(nb_classes):
                cardinality_stats['sum'][k] += real_results[dd]['cardinality'][k] if k in real_results[dd][
                    'cardinality'] else 0

        cardinality_stats['min'] = numpy.min(cardinality_stats['sum'])
        cardinality_stats['max'] = numpy.max(cardinality_stats['sum'])
        cardinality_stats['total'] = numpy.sum(cardinality_stats['sum'])

        cardinality_stats['min_p'] = round((cardinality_stats['min'] * 100.0) / cardinality_stats['total'])
        cardinality_stats['max_p'] = round((cardinality_stats['max'] * 100.0) / cardinality_stats['total'])

        widths = numpy.abs(numpy.diff(edges))
        cardinality_stats['bin_median'] = numpy.median(widths)
        cardinality_stats['bin_avg'] = numpy.average(widths)
    else:
        cardinality_stats = None

    return real_results, cardinality_stats


def print_stats(results, cardinality_stats, label, config, dataset):
    if PRINT_INDIVIDUAL:
        for d in config.load_paths('data.' + dataset + '_paths'):
            dd = os.path.splitext(os.path.basename(d))[0]
            print('%s (%d samples total)\n' % (dd.upper(), results[dd]['length']))

            if COUNT_CARDINALITY:
                print('%s categories cardinality:' % (label,))
                pprint.pprint(results[dd]['cardinality'])

                if COUNT_TRANSITIONS:
                    print('%s neighbouring categories transitions:' % (label,))
                    pprint.pprint(results[dd]['transitions'])

            print('\n')

    if PRINT_CARDINALITY and cardinality_stats:
        print('%s cardinality stats' % (label,))
        pprint.pprint(cardinality_stats)

        print('\n')


def count_transitions(nonzero, numerical_categories):
    transitions = {}
    for val in nonzero:
        if val + 1 in nonzero:
            val_inds = numpy.nonzero(numerical_categories[:-1] == val)
            inc_val_inds = numpy.add(val_inds[0], 1)
            from_count = numpy.count_nonzero(numerical_categories[(inc_val_inds,)] == (val + 1))

            val_inds = numpy.nonzero(numerical_categories[:-1] == (val + 1))
            inc_val_inds = numpy.add(val_inds[0], 1)
            to_count = numpy.count_nonzero(numerical_categories[(inc_val_inds,)] == val)

            if from_count > 0 or SHOW_ZERO_TRANSITIONS:
                transitions['%02d -> %02d' % (val, val + 1)] = from_count
            if to_count > 0 or SHOW_ZERO_TRANSITIONS:
                transitions['%02d -> %02d' % (val + 1, val)] = to_count
    return transitions


def parse_data(args):
    parser = argparse.ArgumentParser(
        description="Data stats helper script")

    parser.add_argument(
        '-d',
        '--dataset',
        type=readable_file,
        metavar='DATASET_NAME',
        default='train',
        choices=['train', 'val', 'test']
    )

    parser.add_argument(
        '-e',
        '--edges',
        type=readable_file,
        metavar='EDGES_FILE',
        default=None
    )

    parser.add_argument(
        '-s',
        '--setup',
        type=readable_file,
        metavar='SETUP_FILE',
        default=None
    )

    parser.add_argument(
        '-c',
        '--config',
        type=readable_file,
        metavar='CONFIG_FILE',
        default=None
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_data(sys.argv[1:])
    run(args.dataset, args.config, args.edges, args.setup)
