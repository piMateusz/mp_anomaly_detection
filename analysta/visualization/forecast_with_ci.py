import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error


def plot_moving_average(real_y, pred_y, window, scale=1.96, plot_anomalies=False):

    """
        real_y - time series
        pred_y - predictions
        window - confidence intervals window size
        plot_intervals - show confidence intervals
        plot_anomalies - show anomalies

    """
    plt.figure(figsize=(15, 5))
    plt.title("Moving average\n window size = {}".format(window))
    plt.plot(pred_y, "g", label="Rolling mean trend")

    # Plot confidence intervals for smoothed values
    mae = mean_absolute_error(real_y[window:], pred_y[window:])
    deviation = np.std(real_y[window:] - pred_y[window:])
    lower_bond = pred_y - (mae + scale * deviation)
    upper_bond = pred_y + (mae + scale * deviation)
    plt.plot(upper_bond, "r--", label="Upper Bond / Lower Bond")
    plt.plot(lower_bond, "r--")

    # Having the intervals, find abnormal values
    if plot_anomalies:
        anomalies = real_y[:]
        anomalies[real_y < lower_bond] = real_y[real_y < lower_bond]
        anomalies[real_y > upper_bond] = real_y[real_y > upper_bond]
        plt.plot(anomalies, "ro", markersize=10)

    plt.plot(real_y[window:], label="Actual values")
    plt.legend(loc="upper left")
    plt.grid(True)
    plt.show()