import os

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def plot_percent_change(analyzed_channels, channels_names, data, data_mean, results_dir, filename='percent_change.pdf',
                        threshold=1.0):
    percent_change = np.abs((data_mean - data) / data_mean) * 100.0
    with PdfPages(os.path.join(results_dir, filename)) as pdf:
        for i, ch in enumerate(analyzed_channels):
            fig = plt.figure()
            fig.suptitle('{}: mean={:.4f}'.format(channels_names[ch], data_mean[i]))
            plt.bar(np.arange(len(percent_change))+1, percent_change[:, i])
            plt.axhline(threshold, linestyle='--', color='k')
            plt.ylabel('change from mean [%]')
            plt.xlabel('batch no')
            pdf.savefig(fig)
            plt.close()
