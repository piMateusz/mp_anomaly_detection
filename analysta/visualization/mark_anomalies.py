import argparse
import sys

from matplotlib import pyplot as plt
from matplotlib.patches import ConnectionPatch

from analysta.utils import InfrequentWriter, get_results_path
from analysta.visualization.utils import get_channels_info
from analysta.utils.config import Config
from analysta.cli.arguments.common import add_config_argument, add_limit_argument, add_results_argument
from analysta.data.loading import load_series_from_paths, get_series_loader, extract_series_name_from_path


import logging


def plot_spans(spans, axes, offset=0):
    for span in spans:
        for ax in axes:
            ax.axvspan(span[0] if offset < span[0] else offset, span[1], facecolor='lightgray', alpha=0.5)


def plot_mark(mark, axes):
    con = ConnectionPatch((mark, axes[-1].get_ylim()[0]), (mark, axes[0].get_ylim()[1]),
                          "data", "data", axes[-1], axes[0],
                          linewidth=1, arrowstyle='-', color='r', connectionstyle='angle3', alpha=1)
    axes[-1].add_artist(con)


def onclick(series_name, clicked_points, marks_writer, axes, offset, event):
    if event.xdata is not None:
        clicked_points.append(int(round(event.xdata)))
        print('Added {}'.format(clicked_points[-1]))
        plot_mark(clicked_points[-1], axes)
        plt.draw()

        if len(clicked_points) % 2 == 0:
            plot_spans([(
                clicked_points[-2],
                clicked_points[-1]
            )], axes, offset[0])
            plt.draw()
            marks_writer.write({
                'name': series_name,
                'start': clicked_points[-2],
                'stop': clicked_points[-1],
                'type': 'hand_mark'
            })


def onkey(pause, event):
    if event.key == ' ':
        pause[0] = not pause[0]


def plot_series(raw_data, channels, channels_names, series_name, marks_writer, samples_limit=int(1e6)):
    fig = plt.figure()
    axes = fig.subplots(len(channels), 1, sharex='all')

    clicked_points = []
    pause = [False]
    offset = [0]

    total_samples = len(raw_data)
    step = int(samples_limit/2)

    if len(axes) < 2:
        axes = (axes,)

    axes[-1].set_xlabel('samples')

    cid = fig.canvas.mpl_connect('button_press_event',
                                 lambda event: onclick(
                                     series_name, clicked_points, marks_writer, axes, offset,
                                     event
                                 ))
    kid = fig.canvas.mpl_connect('key_press_event',
                                 lambda event: onkey(
                                     pause,
                                     event
                                 ))

    plt.show(False)

    for i, samples_offset in enumerate(range(0, total_samples - step, step)):
        offset[0] = samples_offset

        fig.suptitle('{}; offset={}'.format(series_name, samples_offset))

        stop = int(samples_limit+samples_offset)
        x = range(samples_offset, stop)

        for row, c in enumerate(channels):
            ax = axes[row]
            ax.clear()
            data_length = len(raw_data[samples_offset:stop, c])
            ax.plot(x[:data_length], raw_data[samples_offset:stop, c], label=channels_names[c])

        for ax in axes:
            ax.legend()

        plt.draw()
        plt.pause(3)

        while pause[0]:
            plt.pause(1)
            if not plt.fignum_exists(fig.number):
                pause[0] = False

        if not plt.fignum_exists(fig.number):
            break

    fig.canvas.mpl_disconnect(cid)
    fig.canvas.mpl_disconnect(kid)
    fig.clear()
    plt.close(fig)


def run(config_path, limit=None, data_set='train', samples_limit=int(1e6), results_dir='results'):
    config = Config(config_path)
    series_loader = get_series_loader(config)
    marks_path = get_results_path('marks', subdir_name=results_dir)
    marks_writer = InfrequentWriter(marks_path, fieldnames=['name', 'start', 'stop', 'type'])

    data_paths = config.load_paths('data.{:s}_paths'.format(data_set))[:limit]
    channels_names, input_channels, output_channels = get_channels_info(config)
    raw_data, series_names = load_series_from_paths(data_paths, series_loader)

    for name, series in zip(series_names, raw_data):
        plot_series(series, input_channels, channels_names, name, marks_writer, samples_limit=samples_limit)


def parse_data(args):
    parser = argparse.ArgumentParser(
        description="Raw data visualization")

    add_config_argument(parser)
    add_limit_argument(parser, help_text='series limit', metavar='SERIES_LIMIT')
    add_results_argument(parser)

    parser.add_argument(
        '-n',
        '--samples-limit',
        type=int,
        default=int(1e6),
        help='plotted samples per plot limit'
    )
    
    parser.add_argument(
        '-d',
        '--dataset',
        type=str,
        default='train'
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)

    args = parse_data(sys.argv[1:])

    run(args.config, args.limit, args.dataset, args.samples_limit, args.results_dir)
