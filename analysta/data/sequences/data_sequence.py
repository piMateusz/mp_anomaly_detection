import logging

import numpy as np
from sklearn.preprocessing import OneHotEncoder

from analysta.data.analysis.mean_and_var import update_mean_and_variance_for_dataset, get_mean_and_variance_for_batch
from analysta.data.sequences.readers import WindowsReader, RawReader

try:
    from keras.utils import Sequence
except ImportError:
    from collections import Sequence


class DataSequence(Sequence):
    def __init__(self, data, config, samples_percentage=1, shuffle=True, name='default', return_indexes=False,
                 dataset_mean=None, dataset_var=None, anomaly_marks=None, **kwargs):

        self._config = config
        self._logger = logging.getLogger(__name__)

        self._input_channels = kwargs.get('input_channels', self._config['data.input_channels'])
        self._output_channels = kwargs.get('output_channels', self._config.get('data.output_channels', []))
        self._labels_channel = self._config.get('data.labels_channel',
                                                self._output_channels[0] if len(self._output_channels) else 'marks')

        self.__return_labels = False

        self.__use_one_hot = False
        self.__one_hot_encoder = None

        self.__return_indexes = return_indexes

        self.__normalize = kwargs.get('normalize', self._config.get('preparation.normalize', True))

        self.__normalize_counter = None
        self.__dataset_mean = dataset_mean
        self.__dataset_var = dataset_var
        self.__dataset_size = None
        self.__dataset_y_mean = None

        self.__current_batch = None

        reader_kwargs = {
            'data': data,
            'in_channels': self._input_channels,
            'out_channels': self._output_channels,
            'labels_channel': self._labels_channel,
            'look_back': self._config['preparation.look_back'],
            'chunk_size': self._config.setdefault('preparation.chunk_size', 1),
            'batch_size': self._config.setdefault('preparation.batch_size', 1024),
            'samples_percentage': samples_percentage,
            'name': name,
            'return_indexes': return_indexes
        }

        if not len(data) or data[0].ndim == 2:  # samples x channels
            reader_kwargs.update({
                'look_ahead': self._config.setdefault('preparation.look_ahead', 1),
                'required_history': self._config.setdefault('preparation.required_history', None),
                'shuffle': shuffle,
                'allow_random': self._config.setdefault('preparation.allow_random_batch', False),
                'anomaly_marks': anomaly_marks
            })
            self._reader = RawReader(**reader_kwargs)
        else:  # windows x look_back+1 x channels
            self._reader = WindowsReader(**reader_kwargs)

        # needs to be after _logger
        self._shared_channels = self._get_shared_channels_indexes(self._input_channels, self._output_channels)

        self.setup_one_hot(self._config.get('data.categories', {}))

    def __len__(self):
        return len(self._reader)

    def __calc_item(self, idx):
        out = self._reader.calc_item(idx)

        data_x, data_y = self._postprocess_batch(out[0], out[1])

        if self.use_one_hot:
            data_y = self.to_one_hot(data_y)

        if self.__return_labels:
            ret_y = (data_y, out[2].squeeze().astype(bool))
        else:
            ret_y = data_y

        if self.__return_indexes:
            return data_x, ret_y, out[3]
        else:
            return data_x, ret_y

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return self.__items_genarator(indices)
        else:
            return self.__calc_item(item)

    def __items_genarator(self, indices):
        for i in range(*indices):
            self.__current_batch = self.__calc_item(i)
            yield self.__current_batch

    def _postprocess_batch(self, batch_x, batch_y):
        if self.__normalize:
            return self.__studentize_batch(batch_x, batch_y)
        else:
            return batch_x, batch_y

    def __studentize_batch(self, batch_x, batch_y):
        if self.__dataset_mean is None or self.__dataset_var is None:
            self._logger.warning("DataSequence '{}': dataset mean or variance not available, "
                                 "cannot studentize. Did you forgot to set them or call "
                                 "sequence.reset_normalize?".format(self._reader.name))
            return batch_x, batch_y

        if self.__normalize_counter is not None and self.__normalize_counter < len(self):
            self.__normalize_counter += 1
            x_batch_mean, x_batch_var = get_mean_and_variance_for_batch(batch_x)
            self.__dataset_mean[:], self.__dataset_var[:], self.__dataset_size = update_mean_and_variance_for_dataset(
                self.__dataset_mean, self.__dataset_var, self.__dataset_size,
                x_batch_mean, x_batch_var, batch_x.shape[0]
            )

            # reset output channels mean - it needs to be calculated anew
            self.__dataset_y_mean = None

        dataset_std = np.sqrt(self.__dataset_var)

        studentized_x = np.nan_to_num((batch_x - self.__dataset_mean) / dataset_std)

        if batch_y.size:
            std_for_y = np.where(self._shared_channels > -1, dataset_std[self._shared_channels], 1)

            studentized_y = np.nan_to_num((batch_y - self.dataset_y_mean) / std_for_y)
        else:
            studentized_y = batch_y

        return studentized_x, studentized_y

    def _get_shared_channels_indexes(self, in_channels, out_channels):
        indexes = []
        for ch in out_channels:
            try:
                idx = in_channels.index(ch)
            except ValueError:
                idx = -1
                if self.__normalize:
                    self._logger.warning(
                        "DataSequence '{}': the out_channel {} is not in in_channels. It will not be normalized!".format(
                            self._reader.name,
                            ch
                        )
                    )
            indexes.append(idx)
        return np.array(indexes)

    @property
    def batch_size(self):
        return self._reader.batch_size

    @property
    def total_samples(self):
        return self._reader.total_samples

    @property
    def dtype(self):
        return self._reader.dtype

    @property
    def shape_x(self):
        return self._reader.shape_x

    @property
    def shape_y(self):
        shape = self._reader.shape_y
        if self.use_one_hot:
            max_length = self.get_max_categories_length(self.__one_hot_encoder.categories)
            return shape[0], max_length
        else:
            return shape

    @property
    def dataset_mean(self):
        return self.__dataset_mean

    @dataset_mean.setter
    def dataset_mean(self, value):
        self.__dataset_mean = value
        self.__normalize_counter = None

    @property
    def dataset_var(self):
        return self.__dataset_var

    @dataset_var.setter
    def dataset_var(self, value):
        self.__dataset_var = value
        self.__normalize_counter = None

    @property
    def dataset_y_mean(self):
        if self.__dataset_y_mean is None and len(self._shared_channels):
            self.__dataset_y_mean = np.where(self._shared_channels > -1, self.__dataset_mean[self._shared_channels], 0)
        return self.__dataset_y_mean

    @property
    def use_one_hot(self):
        return self.__use_one_hot

    def setup_one_hot(self, maybe_categories):
        if isinstance(maybe_categories, dict) and len(maybe_categories.keys()) > 0:
            self.__use_one_hot = True
            categories = [
                sorted(maybe_categories[str(ch)].values())
                for ch in self._output_channels
            ]
            max_length = self.get_max_categories_length(categories)

            self.__one_hot_encoder = OneHotEncoder(categories=categories, sparse=False)
            self.__one_hot_encoder.fit([
                [c[i % len(c)] for c in categories]
                for i in range(max_length)
            ])
        else:
            self.__use_one_hot = False
            self.__one_hot_encoder = None

    @staticmethod
    def get_max_categories_length(categories):
        lengths = [len(l) for l in categories]
        if len(lengths) > 1:
            max_length = max(*lengths)
        else:
            max_length = lengths[0]
        return max_length

    def to_one_hot(self, data_y):
        return self.__one_hot_encoder.transform(data_y).astype(bool)

    def unwrap_one_hot(self, data_y):
        return self.__one_hot_encoder.inverse_transform(data_y)

    @property
    def return_labels(self):
        return self.__return_labels

    @return_labels.setter
    def return_labels(self, value):
        self.__return_labels = bool(value)

    @property
    def chunk_size(self):
        return self._reader.chunk_size

    @chunk_size.setter
    def chunk_size(self, value):
        self._reader.chunk_size = value

    @property
    def usable_samples(self):
        return self._reader.usable_samples

    @property
    def current_out_indexes(self):
        return self._reader.current_out_indexes

    @property
    def current_batch(self):
        return self.__current_batch

    def reset_normalize(self):
        self.__normalize_counter = 0
        self.__dataset_mean = np.zeros((self.shape_x[1],))
        self.__dataset_var = np.zeros((self.shape_x[1],))
        self.__dataset_y_mean = None
        self.__dataset_size = 0

    def sync_with(self, other_data_sequence):
        self.dataset_mean = other_data_sequence.dataset_mean
        self.dataset_var = other_data_sequence.dataset_var
        self.return_labels = other_data_sequence.return_labels
        self.chunk_size = other_data_sequence.chunk_size

