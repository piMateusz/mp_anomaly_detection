import numpy

from .grided_data_sequence import GridedDataSequence


class FlatDataSequence(GridedDataSequence):
    def _postprocess_batch(self, batch_x, batch_y):
        batch_x, batch_y = super(FlatDataSequence, self)._postprocess_batch(batch_x, batch_y)
        return batch_x.reshape((-1, numpy.prod(batch_x.shape[1:]))), batch_y
