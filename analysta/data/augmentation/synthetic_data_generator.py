import numpy
import argparse
#numpy.set_printoptions(threshold=numpy.nan)


def insert_arbitrary_signal_into_original(position, length, original_signal, indices):
    """
    Generate pulse anomaly in the original signal.
    Args:
        position:
        length:
        original_signal:
        indices:

    Returns:

    """
    arbitrary_signal = numpy.array([[]])
    # TODO: provide condition for position and length with respect to original_signal

    for i in range(length):
        original_signal[position + i] = 1
        indices[position + i] = 1

    return original_signal, indices


def insert_single_pulse_into_original(position, length, original_signal, indices):
    """
    Generate pulse anomaly in the original signal.
    Args:
        position:
        length:
        original_signal:
        indices:

    Returns:

    """
    # TODO: provide condition for position and length with respect to original_signal

    for i in range(length):
        original_signal[position + i] = 1
        indices[position + i] = 1

    return original_signal, indices


def insert_multiple_pulses_into_original(original_signal):
    """

    Args:
        original_signal:

    Returns:

    """
    START_POSITION=100
    IMPULSE_LENGTH=50
    ANOMALY_AMOUNT=1000
    SPACE_BETWEEN_ANOMALIES=1050

    # TODO: provide conditions

    for i in range(ANOMALY_AMOUNT):
        if not i:  # for the first iteration
            original, indices = insert_single_pulse_into_original(
                START_POSITION + i * (SPACE_BETWEEN_ANOMALIES + IMPULSE_LENGTH),
                IMPULSE_LENGTH,
                original_signal,
                numpy.zeros_like(original_signal))
        else:
            original, indices = insert_single_pulse_into_original(
                START_POSITION + i * (SPACE_BETWEEN_ANOMALIES + IMPULSE_LENGTH),
                IMPULSE_LENGTH,
                original,
                indices)

    return original, indices


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Provide file name with original data')
    parser.add_argument('file_name', type=str, help='file name')
    args = parser.parse_args()

    # load data from the input file
    data = numpy.load(args.file_name)
    data = data['data'].T
    data_first_row = data[0]

    result = insert_multiple_pulses_into_original(data_first_row)

    concatenated_results = numpy.concatenate((
         result[0].reshape((1, data.shape[1])),
         data[1:],
         result[1].reshape((1, data.shape[1])),
         ),
        axis=0)

    output_file_name = args.file_name.split('/')[-1].split('.')[0] + '_synth'
    numpy.savez(output_file_name, data=concatenated_results.T)

    print("Done !")
    print("The file: '{}.npz' has been generated.".format(output_file_name))




