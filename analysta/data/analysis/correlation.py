import numpy as np


def correlation(series, analyzed_channels):
    if (analyzed_channels is None) or (len(analyzed_channels) == 0):
        analyzed_channels = slice(None)
    data = series[:, analyzed_channels]
    corr = np.corrcoef(data.T)
    print("Correlation: ", corr)


