import numpy as np


def get_mean_and_variance_for_batch(batch):
    axes = tuple(range(batch.ndim-1))
    channels = batch.shape[-1]
    batch_means = np.mean(batch, axis=axes)
    batch_vars = np.mean((batch - batch_means.reshape(tuple([1] * len(axes) + [channels]))) ** 2, axis=axes)

    return batch_means, batch_vars


def update_mean_and_variance_for_dataset(dataset_mean, dataset_var, dataset_size, batch_mean, batch_var, batch_size):
    # https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Parallel_algorithm

    updated_size = dataset_size + batch_size

    delta = batch_mean - dataset_mean
    m_a = dataset_var * dataset_size
    m_b = batch_var * batch_size
    M2 = m_a + m_b + delta ** 2 * dataset_size * batch_size / updated_size

    updated_var = M2 / updated_size
    updated_mean = (dataset_size * dataset_mean + batch_size * batch_mean) / updated_size

    return updated_mean, updated_var, updated_size


def get_mean_and_variance_for_dataset(batches_means, batches_vars, batches_sizes):
    out_var = batches_vars[0]
    out_mean = batches_means[0]
    out_size = batches_sizes[0]

    for i in range(1, len(batches_sizes)):
        out_mean, out_var, out_size = update_mean_and_variance_for_dataset(out_mean, out_var, out_size,
                                                                           batches_means[i], batches_vars[i],
                                                                           batches_sizes[i])

    return out_mean, out_var
