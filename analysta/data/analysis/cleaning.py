import logging

import pandas as pd


def fix_missing_in_series(series, analyzed_channels=None):
    if analyzed_channels is None or (analyzed_channels is not None and len(analyzed_channels) == 0):
        analyzed_channels = slice(None)
    dataframe = pd.DataFrame(data=series[:, analyzed_channels]).infer_objects()

    logging.getLogger(__name__).info("{} data is NaN, which is {}%".format(
        dataframe.isna().sum(),
        dataframe.isna().sum() / len(dataframe)
    ))

    dataframe = dataframe.interpolate(method="linear").fillna(method="bfill").fillna(method="ffill")

    return dataframe.to_numpy()


