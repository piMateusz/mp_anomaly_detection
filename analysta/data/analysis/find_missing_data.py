import os
import sys

import numpy as np
import csv

from tqdm import trange


def col_hist(data, missing_threshold_low, missing_threshold_high):
    """

    :param data:
    :param missing_threshold_low:
    :param missing_threshold_high: set to -1 to ignore (in the config)
    :return:
    """
    hist = []
    counter = 0
    start = 0
    to_be_removed = False

    for i in range(len(data)):
        if np.isnan(data[i]):
            if counter == 0:
                start = i
            counter += 1
        else:
            if counter > 0:
                if counter > missing_threshold_low:
                    hist.append((start, counter, i))
                    if (missing_threshold_high != -1) and (counter > missing_threshold_high):
                        to_be_removed = True

                counter = 0

    #  in the case when the element is at the very end of the list
    if counter > 0:
        hist.append((start, counter, i))
        if (missing_threshold_high != -1) and (counter > missing_threshold_high):
            to_be_removed = True

    return hist, to_be_removed


def find_missing_data(series_name,
                      series,
                      analyzed_channels,
                      missing_threshold_low,
                      missing_threshold_high,
                      results_dir='results'):
    if (analyzed_channels is None) or (len(analyzed_channels) == 0):
        analyzed_channels = slice(None)
    data = series[:, analyzed_channels]

    if not isinstance(analyzed_channels, list):
        analyzed_channels = list(range(data.shape[1]))

    out = {}

    with open(os.path.join(results_dir, series_name + '_missing_stats.csv'), mode='w') as missing_file:
        missing_file_writer = csv.writer(missing_file, delimiter=',')
        for i in trange(0, data.shape[1], file=sys.stdout):
            hist, to_be_removed = col_hist(
                            data=data[:, i],
                            missing_threshold_low=missing_threshold_low,
                            missing_threshold_high=missing_threshold_high)
            if not hist:  # check if the hist is empty
                missing_file_writer.writerow([analyzed_channels[i], 'complete'])
                out[analyzed_channels[i]] = True
            elif to_be_removed:
                missing_file_writer.writerow([analyzed_channels[i], 'remove'])
                out[analyzed_channels[i]] = False
            else:
                missing_file_writer.writerow([analyzed_channels[i]] + hist)
                out[analyzed_channels[i]] = hist

    return out




