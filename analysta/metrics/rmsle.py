import numpy as np
from sklearn.metrics import mean_squared_log_error


def root_mean_squared_log_error(y_true, y_pred):
    return np.sqrt(mean_squared_log_error(y_true, y_pred))
