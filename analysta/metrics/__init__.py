# simplified implementations
from .rmse import root_mean_squared_error
from .mape import mean_absolute_percentage_error
from .rmsle import root_mean_squared_log_error


HUMANIZE_METRICS = {
    'loss': {  # not exactly a metric, but...
        "short": "Loss",
        "long": "Loss"
    },
    'support': {
        "short": "Support",
        "long": "Support"
    },
    'confusion_matrix': {
        "short": "Confusion Matrix",
        "long": "Confusion Matrix"
    },

    'r2': {
        "short": "R^2",
        "long": "R^2"
    },
    'mae': {
        "short": "MAE",
        "long": "Mean Absolute Error"
    },
    'med_ae': {
        "short": "MedAE",
        "long": "Median Absolute Error"
    },
    'mse': {
        "short": "MSE",
        "long": "Mean Squared Error"
    },
    'mean_squared_error': {
        "short": "MSE",
        "long": "Mean Squared Error"
    },
    'rmse': {
        "short": "RMSE",
        "long": "Root Mean Squared Error"
    },
    'msle': {
        "short": "MSLE",
        "long": "Mean Squared Log Error"
    },
    'mape': {
        "short": "MAPE",
        "long": "Mean Absolute Percentage Error"
    },
    'acc': {
        "short": "Acc",
        "long": "Accuracy"
    },
    'precision': {
        "short": "PPV",
        "long": "Precision (Positive Predictive Value)"
    },
    'for': {
        "short": "FOR",
        "long": "False Omission Rate"
    },
    'fdr': {
        "short": "FDR",
        "long": "False Discovery Rate"
    },
    'npv': {
        "short": "NPV",
        "long": "Negative Predictive Value"
    },
    'recall': {
        "short": "TPR",
        "long": "Recall (True Positive Rate; Sensitivity; Probability of detection; Hit Rate)"
    },
    'tnr': {
        "short": "TNR",
        "long": "Specificity (Selectivity; True Negative Rate)"
    },
    'fpr': {
        "short": "FPR",
        "long": "False Positive Rate (Fall-out; Probability of false alarm)"
    },
    'fnr': {
        "short": "FNR",
        "long": "False Negative Rate (Miss rate)"
    },
    'f1score': {
        "short": "F1",
        "long": "F_1-score"
    },
    'f2score': {
        "short": "F2",
        "long": "F_2-score"
    },
    'cosine_proximity': {
        "short": "Cosine proximity",
        "long": "Cosine proximity"
    },
    'average_precision_score': {
        "short": "APS",
        "long": "Average Precision Score"
    }
}
