import warnings
from collections import OrderedDict
import numpy
from scipy import sparse


def regression_metrics(observed_samples, ss_tot, ss_reg, ss_res, s_res, ss_log_res, s_frac, per_channel=False):
    with warnings.catch_warnings():
        warnings.filterwarnings('error')

        metrics = OrderedDict()

        try:
            metrics['r2'] = 1 - (ss_res / ss_tot)
        except (Warning, ValueError):
            pass

        try:
            metrics['mae'] = s_res / observed_samples
        except (Warning, ValueError):
            pass

        try:
            metrics['mse'] = ss_res / observed_samples
        except (Warning, ValueError):
            pass

        try:
            metrics['rmse'] = numpy.sqrt(metrics['mse'])
        except (Warning, ValueError):
            pass

        try:
            metrics['msle'] = ss_log_res / observed_samples
        except (Warning, ValueError, TypeError):  # we could have received None from partials
            pass

        try:
            metrics['rmsle'] = numpy.sqrt(metrics['msle'])
        except (Warning, ValueError, AttributeError):  # MSLE can be None
            pass

        try:
            metrics['mape'] = s_frac * 100.0 / observed_samples
        except (Warning, ValueError):
            pass

        if not per_channel:
            for key, val in metrics.items():
                metrics[key] = val.mean() if val is not None else val

    return metrics


def classification_metrics(confusion_matrix, average=None, pos_label=1):
    """
    Classification metrics for single channel
    :param pos_label: int index of the positive label in the binary classification, by default 1
    :param confusion_matrix: numpy.ndarray as returned by sklearn.metrics.confusion_matrix
    :param average: (str, None) metrics averaging method, like in sklearn.metrics.precision_recall_fscore_support
        (but with 'samples' not supported)
    :return:
    """

    metrics = OrderedDict()

    support = confusion_matrix.sum(axis=1)
    total_population = confusion_matrix.sum()
    true_positives = confusion_matrix.diagonal()
    predicted_condition_positive = confusion_matrix.sum(axis=0)

    predicted_condition_negative = total_population - predicted_condition_positive
    false_negatives = support - true_positives
    true_negatives = predicted_condition_negative - false_negatives
    false_positives = predicted_condition_positive - true_positives
    condition_positives = true_positives + false_negatives
    condition_negatives = false_positives + true_negatives

    if average == 'micro':
        true_positives = true_positives.sum()
        true_negatives = true_negatives.sum()
        false_positives = false_positives.sum()
        false_negatives = false_negatives.sum()
        predicted_condition_positive = predicted_condition_positive.sum()
        predicted_condition_negative = predicted_condition_negative.sum()
        condition_positives = condition_positives.sum()
        condition_negatives = condition_negatives.sum()

    # avoid division by zero errors
    zeros_to_nan(predicted_condition_positive)
    zeros_to_nan(predicted_condition_negative)
    zeros_to_nan(condition_positives)
    zeros_to_nan(condition_negatives)

    metrics['precision'] = true_positives / predicted_condition_positive
    metrics['for'] = false_negatives / predicted_condition_negative
    metrics['fdr'] = false_positives / predicted_condition_positive
    metrics['npv'] = true_negatives / predicted_condition_negative
    metrics['recall'] = true_positives / condition_positives
    metrics['tnr'] = true_negatives / condition_negatives
    metrics['fpr'] = false_positives / condition_negatives
    metrics['fnr'] = false_negatives / condition_positives
    metrics['f1score'] = fbeta_score(1, metrics['precision'], metrics['recall'])
    metrics['f2score'] = fbeta_score(2, metrics['precision'], metrics['recall'])

    for key, val in metrics.items():
        nan_to_zeros(val)

    if average in ['macro', 'weighted', 'balanced']:
        if average == 'macro':
            weights = numpy.ones_like(support)
        elif average == 'balanced':
            weights = compute_balanced_weights(confusion_matrix.shape[0], support, total_population)
        else:
            weights = support

        for key, val in metrics.items():
            if val is not None:
                metrics[key] = numpy.average(metrics[key], weights=weights)
    elif average == 'binary':
        for key, val in metrics.items():
            if val is not None:
                metrics[key] = metrics[key][pos_label]

    metrics['support'] = support.tolist()
    metrics['acc'] = true_positives.sum() / total_population

    cfm = sparse.coo_matrix(confusion_matrix)
    metrics['confusion_matrix'] = {
        'data': cfm.data.tolist(),
        'row': cfm.row.tolist(),
        'col': cfm.col.tolist()
    }

    return metrics


def zeros_to_nan(a):
    a[numpy.argwhere(a == 0)] = numpy.nan
    return a


def nan_to_zeros(a):
    a[numpy.argwhere(numpy.isnan(a))] = 0
    return a


def binary_classification_metrics():
    # cannot compute this from currently gathered partials
    return {}


# metrics helpers
# ----------------------------------------------------------------------

def fbeta_score(beta, precision, recall):
    pr_sum = precision + recall
    zeros = numpy.nonzero(pr_sum == 0)
    if zeros[0].size:
        precision = zeros_to_nan(precision[:])
        recall = zeros_to_nan(recall[:])

    return (1 + beta ** 2) * (precision * recall) / ((beta ** 2) * precision + recall)


def compute_balanced_weights(num_classes, support, total_population):
    # balanced as in sklearn.utils.class_weight.compute_class_weight
    return total_population / (num_classes * support)


# partials helpers
# ----------------------------------------------------------------------


def total_sum_of_squares(sum_axes, real_y, predicted_y, dataset_y_mean):
    return numpy.sum(
        numpy.square(real_y - dataset_y_mean, dtype=numpy.float128),
        axis=sum_axes
    )


def regression_sum_of_squares(sum_axes, real_y, predicted_y, dataset_y_mean):
    return numpy.sum(
        numpy.square(predicted_y - dataset_y_mean, dtype=numpy.float128),
        axis=sum_axes
    )


def residual_sum_of_squares(sum_axes, real_y, predicted_y, dataset_y_mean):
    return numpy.sum(
        numpy.square(real_y - predicted_y, dtype=numpy.float128),
        axis=sum_axes
    )


def sum_of_residuals(sum_axes, real_y, predicted_y, dataset_y_mean):
    return numpy.sum(
        numpy.abs(real_y - predicted_y, dtype=numpy.float128),
        axis=sum_axes
    )


def log_residual_sum_of_squares(sum_axes, real_y, predicted_y, dataset_y_mean):
    try:
        return numpy.sum(
            numpy.square(
                numpy.log(real_y + 1) - numpy.log(predicted_y + 1),
                dtype=numpy.float128
            ),
            axis=sum_axes
        )
    except RuntimeWarning:  # cannot calculate log
        return numpy.nan


def sum_of_fractionals(sum_axes, real_y, predicted_y, dataset_y_mean):
    try:
        return numpy.sum(
            numpy.abs(1 - numpy.divide(predicted_y, real_y)),
            axis=sum_axes
        )
    except RuntimeWarning:  # cannot calculate log
        return numpy.nan
