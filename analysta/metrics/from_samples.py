import warnings

from scipy import sparse
from sklearn.metrics import r2_score, mean_absolute_error, median_absolute_error, mean_squared_log_error, \
    accuracy_score, precision_recall_fscore_support, confusion_matrix, fbeta_score, average_precision_score, \
    precision_recall_curve, roc_curve, mean_squared_error

from analysta.metrics import root_mean_squared_error, mean_absolute_percentage_error
from analysta.metrics.rmsle import root_mean_squared_log_error


def regression_metrics(real_y, predicted_y):
    warnings.filterwarnings('error')

    metrics = {
        'r2': None,
        'mae': None,
        'med_ae': None,
        'mse': None,
        'rmse': None,
        'msle': None,
        'rmsle': None,
        'mape': None
    }

    try:
        metrics['r2'] = r2_score(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['mae'] = mean_absolute_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['med_ae'] = median_absolute_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['mse'] = mean_squared_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['rmse'] = root_mean_squared_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['msle'] = mean_squared_log_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['rmsle'] = root_mean_squared_log_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    try:
        metrics['mape'] = mean_absolute_percentage_error(real_y, predicted_y)
    except (Warning, ValueError):
        pass

    # TODO: add cosine_proximity metrics compatible with Keras?

    return metrics


def classification_metrics(real_labels, predicted_labels, num_classes):
    metrics = {
        'acc': accuracy_score(
            real_labels,
            predicted_labels
        )
    }

    if num_classes > 1 and predicted_labels.any() and real_labels.any():
        fbeta_average = 'binary' if num_classes == 2 else 'weighted'

        precision, recall, f1score, support = precision_recall_fscore_support(
            real_labels,
            predicted_labels,
            average=fbeta_average
        )

        cfm = sparse.coo_matrix(confusion_matrix(
            real_labels,
            predicted_labels,
            labels=range(num_classes)
        ))

        metrics.update({
            'precision': precision,
            'recall': recall,
            'f1score': f1score,
            'f2score': fbeta_score(
                real_labels,
                predicted_labels,
                beta=2.0,
                average=fbeta_average
            ),
            'confusion_matrix': {
                'data': cfm.data.tolist(),
                'row': cfm.row.tolist(),
                'col': cfm.col.tolist()
            }
        })
    return metrics


def binary_classification_metrics(real_labels, prediction_scores,
                                  calculate_pr_curve, calculate_roc_curve):
    metrics = {}

    if real_labels.any():
        metrics['average_precision_score'] = average_precision_score(
            real_labels, prediction_scores
        )

        if calculate_pr_curve:
            precision_curve, recall_curve, _ = precision_recall_curve(
                real_labels, prediction_scores)
            metrics['pr_curve'] = {
                'recall': recall_curve,
                'precision': precision_curve
            }

        if calculate_roc_curve:
            fpr, tpr, _ = roc_curve(
                real_labels, prediction_scores)
            metrics['roc_curve'] = {
                'fpr': fpr,
                'tpr': tpr
            }

    return metrics
