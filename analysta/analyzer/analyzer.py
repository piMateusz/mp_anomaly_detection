import logging
import math

import numpy

from analysta.data.utils import unwrap_one_hot
from analysta.utils import get_definition_from_path


class Analyzer(object):
    def __init__(self, config, use_one_hot=True):
        self._logger = logging.getLogger(__name__)
        self._chunk_size = config.get('preparation.chunk_size', 1)
        self._window = config.setdefault('analyzer.window', self._chunk_size)

        quotient = self._chunk_size / self._window
        if math.floor(quotient) != quotient or quotient < 1:
            self._logger.warning(
                'Chunk size must be divisible by analyzer window, forcing analyzer.window={}'.format(self._chunk_size)
            )
            self._window = self._chunk_size
            config['analyzer.window'] = self._chunk_size

        self.__ensemble = []
        for plugin in config.get('setup.analyzer_plugins', []):
            plugin_cls = get_definition_from_path(plugin, 'AnalyzerPlugin')
            self.__ensemble.append(plugin_cls(config))

        self.__voting_method = config.setdefault('analyzer.voting_method', 'any')

        if self.__voting_method not in ['any', 'all', 'vote']:
            self._logger.warning('Unknown voting method, falling back to "any".')
            self.__voting_method = 'any'

        if self.__voting_method == 'all':
            self.__voting_quorum = len(self.__ensemble)
        elif self.__voting_method == 'vote':
            self.__voting_quorum = config.setdefault('analyzer.voting_quorum',
                                                     int(math.ceil(len(self.__ensemble) / 2.0)))
        else:
            self.__voting_quorum = 1

        self.__chunk_mode = config.setdefault('analyzer.chunk_mode', 'last')
        if self.__chunk_mode not in ['last', 'any', 'all', 'majority']:
            self._logger.warning('Unknown chunk mode, falling back to "last".')
            self.__chunk_mode = 'last'
            config['analyzer.chunk_mode'] = self.__chunk_mode

        self.__use_one_hot = use_one_hot

    def fit(self, predictions_generator):
        if numpy.array([plugin.needs_fitting for plugin in self.__ensemble]).any():
            # tqdm is already in predictions_generator
            for encoded_real_y, encoded_predicted_y in predictions_generator:
                (real_samples_labels, real_chunks_labels,
                 real_y, predicted_y) = self.__get_y_and_labels(encoded_real_y, encoded_predicted_y, predictions_generator)

                for plugin in self.__ensemble:
                    if plugin.needs_fitting:
                        plugin.fit_on_batch(real_y, predicted_y, real_samples_labels, real_chunks_labels)

            for plugin in self.__ensemble:
                if plugin.needs_fitting:
                    plugin.all_training_data_shown()

    def predict(self, predictions_generator):
        # tqdm is already in predictions_generator
        for encoded_real_y, encoded_predicted_y in predictions_generator:
            (real_samples_labels, real_chunks_labels,
             real_y, predicted_y) = self.__get_y_and_labels(encoded_real_y, encoded_predicted_y, predictions_generator)

            if len(self.__ensemble):
                votes = numpy.vstack([
                    plugin.predict_on_batch(real_y, predicted_y)
                    for plugin in self.__ensemble
                ])

                if votes.ndim == 3:
                    prediction_scores = votes.sum(axis=2)
                else:
                    prediction_scores = votes
                predicted_samples_labels = prediction_scores >= self.__voting_quorum
                prediction_scores = prediction_scores / float(votes.shape[1])
            else:
                if predicted_y.ndim != 4 or predicted_y.shape[2] != 1 or predicted_y.shape[3] < 2:
                    raise ValueError('Predictions returned by model cannot be directly used as anomaly '
                                     'classification. Please specify at least one plugin in setup.analyzer_plugins.')
                predicted_y = numpy.squeeze(predicted_y, axis=2)  # we've checked it is == 1 above
                if predicted_y.shape[2] == 2:  # binary classification, we're only interested in anomaly scores
                    prediction_scores = predicted_y[..., 1]
                else:  # multi-class classification - only return highest score (top1)
                    prediction_scores = numpy.max(predicted_y, axis=2)

                if self.__use_one_hot:
                    predicted_y = predictions_generator.unwrap_one_hot(predicted_y)
                predicted_samples_labels = predicted_y[:]

            predicted_chunk_labels = self.__get_chunks_labels(predicted_samples_labels)

            yield (real_y, predicted_y,
                   real_samples_labels, predicted_samples_labels, prediction_scores,
                   real_chunks_labels, predicted_chunk_labels)

    def __get_y_and_labels(self, real_y, predicted_y, predictions_generator):
        real_y, real_samples_labels = real_y[0], real_y[1]

        if self.__use_one_hot:
            real_y = predictions_generator.unwrap_one_hot(real_y)

        target_real_y_shape = [-1, self._window] + list(real_y.shape[1:])
        if len(target_real_y_shape) < 3:
            target_real_y_shape.append(1)

        target_predicted_y_shape = [-1, self._window] + list(predicted_y.shape[1:])

        # TODO: is this still needed?
        # if len(target_predicted_y_shape) < 4:
        #     target_predicted_y_shape.append(1)

        real_samples_labels = real_samples_labels.reshape([-1, self._window] + list(real_samples_labels.shape[1:]))
        real_chunks_labels = self.__get_chunks_labels(real_samples_labels)

        return (real_samples_labels,
                real_chunks_labels,
                real_y.reshape(target_real_y_shape),
                predicted_y.reshape(target_predicted_y_shape))

    def __get_chunks_labels(self, samples_labels):
        if self.__chunk_mode == 'any':
            chunks_labels = samples_labels.any(axis=1)
        elif self.__chunk_mode == 'all':
            chunks_labels = samples_labels.all(axis=1)
        elif self.__chunk_mode == 'majority':
            chunks_labels = samples_labels.sum(axis=1) > int(samples_labels.shape[1] / 2)
        else:  # default "last"
            chunks_labels = samples_labels[:, -1]
        return chunks_labels

    @property
    def window(self):
        return self._window
