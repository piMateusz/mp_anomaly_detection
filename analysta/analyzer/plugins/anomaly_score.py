import numpy
from scipy.stats import norm

from analysta.analyzer.plugins.plugin import AnalyzerPlugin
from analysta.data.analysis.mean_and_var import get_mean_and_variance_for_batch, update_mean_and_variance_for_dataset


class AnomalyScorePlugin(AnalyzerPlugin):
    """
    Decide if sample is anomalous based on Anomaly Score
    """

    def __init__(self, *args, **kwargs):
        super(AnomalyScorePlugin, self).__init__(*args, **kwargs)

        self.__threshold = self._config.get('analyzer.as.threshold')
        self.__normal_mean = self._config.get('analyzer.as.mean')
        self.__normal_std = self._config.get('analyzer.as.std')

        if self.__threshold is not None and self.__normal_mean is not None and self.__normal_std is not None:
            self._needs_fitting = False

        if self.__threshold is not None:
            self.__threshold = numpy.array(self.__threshold)

        self.__normal_size = 0

        if self.__normal_mean is not None:
            self.__normal_mean = numpy.array(self.__normal_mean)
        else:
            self.__normal_mean = 0

        if self.__normal_std is not None:
            self.__normal_std = numpy.array(self.__normal_std)
        else:
            self.__normal_std = 0

        self.__normal_var = 0
        self.__max_normal_score = 0
        self.__min_anomaly_score = float('Inf')

    def fit_on_batch(self, real_y, predicted_y, real_samples_labels, real_chunks_labels):
        normal_indices = numpy.nonzero(real_samples_labels == 0)
        anomaly_indices = numpy.nonzero(real_samples_labels == 1)

        normal_errors = real_y[normal_indices] - predicted_y[normal_indices]
        anomaly_errors = real_y[anomaly_indices] - predicted_y[anomaly_indices]

        batch_mean, batch_var = get_mean_and_variance_for_batch(normal_errors)
        batch_size = normal_errors.shape[0]

        self.__normal_mean, self.__normal_var, self.__normal_size = update_mean_and_variance_for_dataset(
            self.__normal_mean, self.__normal_var, self.__normal_size,
            batch_mean, batch_var, batch_size
        )
        self.__normal_std = numpy.sqrt(self.__normal_var)

        normal_scores = self.__get_anomaly_scores(normal_errors)
        max_normal_score = normal_scores.max(axis=0)

        self.__max_normal_score = numpy.maximum(self.__max_normal_score, max_normal_score)

        anomaly_scores = self.__get_anomaly_scores(anomaly_errors)
        min_anomaly_score = anomaly_scores.min(axis=0)

        self.__min_anomaly_score = numpy.minimum(self.__min_anomaly_score, min_anomaly_score)

        self.__threshold = (self.__max_normal_score + self.__min_anomaly_score) / 2

    def __get_anomaly_scores(self, errors):
        diff = errors - self.__normal_mean
        scores = []
        for w in diff:
            ch_scores = []
            for ch in range(w.shape[-1]):
                ch_scores.append(numpy.dot(w[:, ch].T, w[:, ch]))
            scores.append(numpy.array(ch_scores) / self.__normal_std)
        scores = numpy.array(scores)
        return scores

    def all_training_data_shown(self):
        self._config['analyzer.as.threshold'] = self.__threshold.tolist()
        self._config['analyzer.as.mean'] = self.__normal_mean.tolist()
        self._config['analyzer.as.std'] = self.__normal_std.tolist()

    def predict_on_batch(self, real_y, predicted_y):
        output_shape = tuple([-1] + list(real_y.shape)[2:])

        errors = (real_y - predicted_y).reshape(output_shape)

        scores = self.__get_anomaly_scores(errors)
        channels_labels = scores > self.__threshold

        samples_labels = self._channels_to_points_labels(channels_labels)

        return samples_labels.reshape(real_y.shape[:2])
