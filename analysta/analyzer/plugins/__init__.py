from .max_squared_error import MaxSquaredErrorPlugin
from .mal import MALPlugin
from .anomaly_score import AnomalyScorePlugin
