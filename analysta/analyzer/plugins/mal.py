import numpy
from scipy.stats import norm

from analysta.analyzer.plugins.plugin import AnalyzerPlugin
from analysta.data.analysis.mean_and_var import get_mean_and_variance_for_batch, update_mean_and_variance_for_dataset


class MALPlugin(AnalyzerPlugin):
    """
    Decide if sample is anomalous based on Maximum Anomaly Likelihood
    """

    def __init__(self, *args, **kwargs):
        super(MALPlugin, self).__init__(*args, **kwargs)

        self.__max_anomaly_likelihood = self._config.get('analyzer.mal.max_anomaly_likelihood')
        self.__normal_mean = self._config.get('analyzer.mal.mean')
        self.__normal_std = self._config.get('analyzer.mal.std')

        if self.__max_anomaly_likelihood is not None and self.__normal_mean is not None and self.__normal_var is not None:
            self._needs_fitting = False

        if self.__max_anomaly_likelihood is not None:
            self.__max_anomaly_likelihood = numpy.array(self.__max_anomaly_likelihood)

        if self.__normal_mean is not None:
            self.__normal_mean = numpy.array(self.__normal_mean)
        else:
            self.__normal_mean = 0

        if self.__normal_std is not None:
            self.__normal_std = numpy.array(self.__normal_std)
            self.__normal_var = self.__normal_std ** 2
        else:
            self.__normal_std = 1
            self.__normal_var = 1

        self.__normal_size = 0

    def fit_on_batch(self, real_y, predicted_y, real_samples_labels, real_chunks_labels):
        if self.__max_anomaly_likelihood is None:
            self.__max_anomaly_likelihood = numpy.zeros((real_y.shape[-1],))

        normal_indices = numpy.nonzero(real_samples_labels == 0)
        anomaly_indices = numpy.nonzero(real_samples_labels == 1)

        normal_errors = real_y[normal_indices] - predicted_y[normal_indices]
        anomaly_errors = real_y[anomaly_indices] - predicted_y[anomaly_indices]

        batch_mean, batch_var = get_mean_and_variance_for_batch(normal_errors)
        batch_size = normal_errors.shape[0]

        self.__normal_mean, self.__normal_var, self.__normal_size = update_mean_and_variance_for_dataset(
            self.__normal_mean, self.__normal_var, self.__normal_size,
            batch_mean, batch_var, batch_size
        )
        self.__normal_std = numpy.sqrt(self.__normal_var)

        if anomaly_errors.size:
            likelihoods = norm.pdf(anomaly_errors[..., -1, :], self.__normal_mean, self.__normal_std)
            batch_max_anomaly_likelihood = likelihoods.max(axis=tuple(range(likelihoods.ndim-1)))
            self.__max_anomaly_likelihood = numpy.maximum(
                self.__max_anomaly_likelihood,
                batch_max_anomaly_likelihood
            )

    def all_training_data_shown(self):
        self._config['analyzer.mal.max_anomaly_likelihood'] = self.__max_anomaly_likelihood.tolist()
        self._config['analyzer.mal.mean'] = self.__normal_mean.tolist()
        self._config['analyzer.mal.std'] = self.__normal_std.tolist()

    def predict_on_batch(self, real_y, predicted_y):
        output_shape = tuple([-1] + list(real_y.shape)[2:])

        errors = (real_y - predicted_y).reshape(output_shape)

        likelihoods = norm.pdf(errors, self.__normal_mean, self.__normal_std)
        channels_labels = likelihoods < self.__max_anomaly_likelihood

        points_labels = self._channels_to_points_labels(channels_labels)
        samples_labels = self._points_to_samples_labels(points_labels)

        return samples_labels.reshape(real_y.shape[:2])
