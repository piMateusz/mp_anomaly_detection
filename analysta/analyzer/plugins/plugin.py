import logging

import numpy


class AnalyzerPlugin(object):
    def __init__(self, config):
        """
        Read from config or initialize settings required by the plugin
        :param config:
        """
        self._config = config
        self._logger = logging.getLogger(__name__)

        self._mode = self._config.get('analyzer.mode', 'majority')
        self._needs_fitting = True

        if self._mode not in ['any', 'all', 'majority']:
            self._logger.warning('Unknown mode, falling back to "majority".')
            self._mode = 'majority'
            self._config['analyzer.mode'] = self._mode

    def fit_on_batch(self, real_y, predicted_y, real_samples_labels, real_chunks_labels):
        raise NotImplementedError()

    def all_training_data_shown(self):
        """
        Save the necessary settings in config
        :return:
        """
        raise NotImplementedError()

    def predict_on_batch(self, real_y, predicted_y):
        raise NotImplementedError()

    @property
    def needs_fitting(self):
        return self._needs_fitting

    # helpers

    @staticmethod
    def _channels_to_points_labels(channels_labels):
        return numpy.any(channels_labels, axis=-1)

    def _points_to_samples_labels(self, points_labels):
        if self._mode == 'any':
            quorum = 1
        elif self._mode == 'all':
            quorum = points_labels.shape[-1]
        else:
            quorum = points_labels.shape[-1] / 2.0

        return points_labels.sum(axis=-1) >= quorum
