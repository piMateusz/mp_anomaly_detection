#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following line in the
entry_points section in setup.cfg:

    console_scripts =
     x2binary = analysta.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""
from __future__ import division, print_function, absolute_import

import argparse
import logging
import sys



from analysta import __version__
from analysta.cli.data.cmd import add_parser_args as add_data_parser_args, main as main_data
from analysta.cli.model.cmd import add_parser_args as add_model_parser_args, main as main_model

__author__ = "Maciej Wielgosz"
__copyright__ = "Maciej Wielgosz"
__license__ = "MIT"


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description='Analysta - modeling and anomaly detection for temporal data')
    parser.add_argument(
        '--version',
        action='version',
        version='analysta {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest='loglevel',
        help='set loglevel to INFO',
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest='loglevel',
        help='set loglevel to DEBUG',
        action='store_const',
        const=logging.DEBUG)

    subparsers = parser.add_subparsers(dest='cmd')

    data_parser = subparsers.add_parser('data',
                                        help='raw data analysis, augmenting and visualization')
    model_parser = subparsers.add_parser('model',
                                         help='signals modelling, anomaly detection and results visualization')

    add_data_parser_args(data_parser)
    add_model_parser_args(model_parser)

    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s: %(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat)


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """

    args = parse_args(args)
    setup_logging(args.loglevel)

    if args.cmd == 'data':
        main_data(args)
    elif args.cmd == 'model':
        main_model(args)


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
