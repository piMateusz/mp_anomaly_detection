class DetectorPlugin(object):
    def __init__(self, detector, logger):
        self._detector = detector
        self._logger = logger

    def data_sequences_available(self):
        pass

    def model_available(self):
        pass

    def fitted_model_available(self, history=None):
        pass

    def analyzer_available(self):
        pass

    def fitted_analyzer_available(self):
        pass

    def predicted_batch_available(self, real_y, predicted_y):
        pass

    def analyzed_batch_available(self, real_y, predicted_y,
                                 real_samples_labels, predicted_samples_labels, prediction_scores,
                                 real_chunks_labels, predicted_chunk_labels):
        pass

    def all_data_predicted(self):
        pass

    def all_data_analyzed(self):
        pass
