from analysta.detector.plugins import DetectorPlugin


class VisualizeBatchPlugin(DetectorPlugin):
    def __init__(self, *args, **kwargs):
        super(VisualizeBatchPlugin, self).__init__(*args, **kwargs)

    def predicted_batch_available(self, real_y, predicted_y):
        if not self._detector.model.is_classifier:  # regression
            import matplotlib.pyplot as plt
            no_of_channels = real_y.shape[-1]
            channel_indices = self._detector.config.get('data.output_channels',
                                                        self._detector.config['data.input_channels'])
            channel_names = self._detector.config.get('data.channels_names',
                                                      ['Ch{}'.format(ch) for ch in channel_indices])
            for ch in range(no_of_channels):
                plt.figure()
                plt.gcf().suptitle(channel_names[channel_indices[ch]])
                plt.plot(real_y[..., ch], label='real')
                plt.plot(predicted_y[..., ch], label='predicted')
                plt.legend(loc='upper left')
                plt.show()
