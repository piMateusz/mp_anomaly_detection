import argparse
import logging
import os

import numpy
import sys

from tqdm import tqdm

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.data.analysis.cleaning import fix_missing_in_series
from analysta.data.loading import load_series_from_paths, get_series_loader
from analysta.utils.config.get_config_instance import get_config_instance


def run_cleaning(config, results_dir, **kwargs):
    logger = logging.getLogger(__name__)
    config = get_config_instance(config)
    analyzed_channels = list(set(config.get('data.input_channels', []) + config.get('data.output_channels', [])))
    series_loader = get_series_loader(config)

    if not os.path.exists(results_dir):
        os.mkdir(results_dir)

    for paths in ['data.train_paths', 'data.val_paths', 'data.test_paths']:
        logger.info('Cleaning {}'.format(paths))

        series_paths = config.load_paths(paths)
        raw_data, series_names = load_series_from_paths(series_paths, series_loader)
        series_amount = len(raw_data)

        with tqdm(total=series_amount, file=sys.stdout) as pbar:
            for name, series in zip(series_names, raw_data):
                pbar.set_postfix(series=name)
                fixed_series = fix_missing_in_series(series, analyzed_channels)
                numpy.save(os.path.join(results_dir, '{}.npy'.format(name)), fixed_series)
                pbar.update(1)


def add_cleaning_parser(subparsers=None):
    description = 'clean (fill the missing parts) the data'

    if subparsers is not None:
        parser = subparsers.add_parser('cleaning', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
