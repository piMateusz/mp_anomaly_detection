import argparse
import logging
import os
import sys
from tqdm import tqdm

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.data.analysis.find_missing_data import find_missing_data
from analysta.data.loading import load_series_from_paths, get_series_loader
from analysta.utils.config.get_config_instance import get_config_instance


def run_missing(config, results_dir, **kwargs):
    logger = logging.getLogger(__name__)
    config = get_config_instance(config)

    analyzed_channels = list(set(config.get('data.input_channels', []) + config.get('data.output_channels', [])))

    series_loader = get_series_loader(config)
    missing_threshold_low = config.get('data.missing_threshold_low', 1)
    missing_threshold_high = config.get('data.missing_threshold_high', -1)

    out = {}

    for paths in ['data.train_paths', 'data.val_paths', 'data.test_paths']:
        logger.info('Analyzing {}'.format(paths))

        series_paths = config.load_paths(paths)
        raw_data, series_names = load_series_from_paths(series_paths, series_loader)
        series_amount = len(raw_data)

        with tqdm(total=series_amount, file=sys.stdout) as pbar:
            for name, series in zip(series_names, raw_data):
                pbar.set_postfix(series=name)
                out[name] = find_missing_data(series_name=name,
                                              series=series,
                                              analyzed_channels=analyzed_channels,
                                              missing_threshold_low=missing_threshold_low,
                                              missing_threshold_high=missing_threshold_high,
                                              results_dir=results_dir)
                pbar.update(1)

    return out


def add_missing_parser(subparsers=None):
    description = 'calculate raw statistics of missing data'

    if subparsers is not None:
        parser = subparsers.add_parser('missing', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
