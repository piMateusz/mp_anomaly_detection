import argparse
import json
import logging
import math
import os
import pprint
import random
import sys

import numpy
from tqdm import tqdm

from analysta.cli.arguments.common import add_config_argument
from analysta.data.loading import load_series_from_paths, get_series_loader
from analysta.utils.config.get_config_instance import get_config_instance


def run_stats(config, anomalies=True, propose_split=False, **kwargs):
    logger = logging.getLogger(__name__)
    config = get_config_instance(config)

    series_loader = get_series_loader(config)

    if anomalies:
        anomalies_channel = config.get('data.labels_channel', config.get('data.output_channels'))

        if anomalies_channel is None:
            logger.critical('Please specify anomalies channel in data.labels_channel or data.output_channels')
            exit(1)

    total_samples = 0
    total_series_info = {}
    out = {}

    for dataset in ['train', 'val', 'test']:
        series_paths = config.load_paths('data.{}_paths'.format(dataset))
        raw_data, series_names = load_series_from_paths(series_paths, series_loader, config.get('data.mmap_mode', 'r'))
        series_amount = len(raw_data)

        series_info = {}
        anomalies_info = {}

        with tqdm(total=series_amount, file=sys.stdout) as pbar:
            for name, series in zip(series_names, raw_data):
                pbar.set_postfix(series=name)

                series_info[name] = len(series)
                if anomalies:
                    anomalies_info[name] = numpy.count_nonzero(series[:, anomalies_channel])

                pbar.update(1)

        series_lengths = list(series_info.values())
        anomalies_lengths = list(anomalies_info.values())
        samples = numpy.sum(series_lengths)
        total_samples += samples

        if series_amount:
            out[dataset] = {
                'no of series': series_amount,
                'no of series with anomalies': numpy.count_nonzero(anomalies_lengths) if anomalies else None,
                'total samples': samples,
                'anomalous samples': numpy.sum(anomalies_lengths) if anomalies else None,
                'series lengths': dict(zip(*numpy.unique(series_lengths, return_counts=True))),
                'anomalous samples per series': dict(zip(*numpy.unique(anomalies_lengths, return_counts=True))) if anomalies else None,
            }
            total_series_info.update(series_info)

    pprint.pprint(out)

    if propose_split:
        testing_samples = math.ceil(0.2 * total_samples)
        validation_samples = math.ceil(0.2 * (total_samples - testing_samples))

        shuffled_series = list(total_series_info.keys())
        random.shuffle(shuffled_series)

        idx = 0
        idx, testing_series = sum_over_series(idx, total_series_info, shuffled_series, testing_samples)
        idx, validation_series = sum_over_series(idx, total_series_info, shuffled_series, validation_samples)

        training_series = shuffled_series[idx:]

        return {
            "train_paths": training_series,
            "val_paths": validation_series,
            "test_paths": testing_series
        }


def sum_over_series(current_idx, series_lengths, shuffled_series, samples):
    current_sum = 0
    series = []
    while current_sum < samples:
        series.append(shuffled_series[current_idx])
        current_sum += series_lengths[shuffled_series[current_idx]]
        current_idx += 1
    return current_idx, series


def add_stats_parser(subparsers=None):
    description = 'compile basic dataset stats'

    if subparsers is not None:
        parser = subparsers.add_parser('stats', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    parser.add_argument(
        '--no-anomalies',
        action='store_false',
        dest='anomalies'
    )
    parser.add_argument(
        '--propose-split',
        action='store_true'
    )
