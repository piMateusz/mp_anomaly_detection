import argparse
import logging
import os
import sys
from tqdm import tqdm

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.data.analysis.correlation import correlation
from analysta.data.loading import load_series_from_paths, get_series_loader
from analysta.utils.config.get_config_instance import get_config_instance


def run_correlation(config, **kwargs):
    logger = logging.getLogger(__name__)
    config = get_config_instance(config)
    analyzed_channels = list(set(config.get('data.input_channels', []) + config.get('data.output_channels', [])))
    series_loader = get_series_loader(config)

    for paths in ['data.train_paths', 'data.val_paths', 'data.test_paths']:
        logger.info('Analyzing {}'.format(paths))

        series_paths = config.load_paths(paths)
        raw_data, series_names = load_series_from_paths(series_paths, series_loader)
        series_amount = len(raw_data)

        with tqdm(total=series_amount, file=sys.stdout) as pbar:
            for name, series in zip(series_names, raw_data):
                pbar.set_postfix(series=name)
                correlation(series, analyzed_channels)
                pbar.update(1)


def add_correlation_parser(subparsers=None):
    description = 'calculates correlation for the series in the set'

    if subparsers is not None:
        parser = subparsers.add_parser('correlation', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    # add_results_argument(parser)
