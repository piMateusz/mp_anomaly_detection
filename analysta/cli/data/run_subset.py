import argparse
import logging
import sys

import humanize
import numpy
import os

from tqdm import tqdm

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.cli.data.utils import setup
from analysta.data.sequences import GridedDataSequence, DataSequence
from analysta.data.utils import convert_to_grid
from analysta.utils import get_results_path
from analysta.utils.config.get_config_instance import get_config_instance


def run_subset(config, results_dir, train_batches=32, validation_batches=8, test_batches=10, grided=False, **kwargs):
    logger = logging.getLogger(__name__)
    config = get_config_instance(config)

    if grided and train_batches == 0 and config.get('preparation.edges_path') is None:
        logger.critical('Generation of grided dataset requires at least one training batch.')
        return

    logger.info('Running subset generation with samples_percentage={}.'.format(
        config.get('preparation.samples_percentage', 1)
    ))

    in_channels = config.get('data.input_channels', [])
    out_channels = config.get('data.output_channels', [])
    analyzed_channels = list(set(in_channels + out_channels))
    channels_names = config.get('data.channels_names', [])
    used_names = [channels_names[ch] if isinstance(ch, int) else 'MARKS' for ch in analyzed_channels]
    logger.info('Used channels: {}'.format(str(used_names)))

    limits = {
        'train': train_batches,
        'val': validation_batches,
        'test': test_batches,
    }

    look_ahead = config.get('preparation.look_ahead', 1)
    if look_ahead != 1:
        logger.warning('Detected invalid preparation.look_ahead (={}). '
                       'Subset generation requires preparation.look_ahead=1, overwriting.')
        config['preparation.look_ahead'] = 1

    # remember original vales
    chunk_size = config.get('preparation.chunk_size', 1)
    look_back = config['preparation.look_back']

    # adjust config for dataset generation
    config['preparation.look_back'] = look_back + chunk_size
    config['preparation.chunk_size'] = 1

    for key, limit in limits.items():
        if limit == 0:
            continue
        elif limit == -1:
            limit = None

        paths = 'data.{}_paths'.format(key)

        batches_limit, data_sequence = setup(config, paths, limit, analyzed_channels, [], True, False, normalize=False)

        shape = ((batches_limit-1) * data_sequence.batch_size + data_sequence[batches_limit-1][0].shape[0],
                 data_sequence.shape_x[0],  # look_back + chunk_size for output
                 data_sequence.shape_x[1])  # len(analyzed_channels)

        logger.info('Amount of samples available in {} subset will be approximately {}.'.format(
            key,
            humanize.intword(batches_limit * data_sequence.batch_size * (data_sequence.shape_x[0] - chunk_size - 1))
        ))

        out_path = os.path.join(results_dir, '{}_set.npy'.format(key))
        logger.info('Generating raw {} subset...'.format(out_path))

        header = {'shape': shape,
                  'fortran_order': False,
                  'descr': numpy.lib.format.dtype_to_descr(data_sequence.dtype)}

        with open(out_path, 'wb') as fp:
            numpy.lib.format.write_array_header_1_0(fp, header)
            offset = fp.tell()

        mmap = numpy.memmap(out_path, dtype=data_sequence.dtype, mode='w+', shape=shape, offset=offset)

        with tqdm(total=batches_limit, file=sys.stdout) as pbar:
            for i, batch in enumerate(data_sequence[:batches_limit]):
                x = batch[0]
                # TODO: handle last (smaller) batch
                mmap[i*data_sequence.batch_size:(i+1)*data_sequence.batch_size] = x
                pbar.update(1)

        mmap.flush()

        with open(out_path, 'r+b') as fp:
            numpy.lib.format.write_array_header_1_0(fp, header)

        logger.info('{} saved.'.format(out_path))

    if grided:
        grided_config = config.copy()

        # re-index channels
        in_ch = tuple([i for i in range(len(analyzed_channels)) if analyzed_channels[i] in in_channels])
        out_ch = tuple([i for i in range(len(analyzed_channels)) if analyzed_channels[i] in out_channels])
        grided_config['data.input_channels'] = in_ch
        grided_config['data.output_channels'] = out_ch

        # restore original dataset look_back/chunk_size
        grided_config['preparation.look_back'] = look_back
        grided_config['preparation.chunk_size'] = chunk_size

        edges_path = config.get('preparation.edges_path')
        if edges_path is None:
            # load train set
            train_sequence = GridedDataSequence(
                [numpy.load(os.path.join(results_dir, 'train_set.npy'))],
                grided_config, shuffle=False
            )
            # find in/out edges
            in_edges = train_sequence.in_edges
            out_edges = train_sequence.out_edges

            edges_path = get_results_path('edges', 'npz', results_dir)
            logger.info('Saving edges...')
            numpy.savez(
                edges_path,
                **{
                    'in_edges': in_edges,
                    'out_edges': out_edges
                }
            )

            in_buckets = train_sequence.in_buckets
            out_buckets = train_sequence.out_buckets
        else:
            edges = numpy.load(edges_path)
            in_edges = edges['in_edges']
            out_edges = edges['out_edges']
            in_buckets = config.get('preparation.in_buckets', 0)
            out_buckets = config.get('preparation.out_buckets', 0)

        # for each dataset convert (and possibly duplicate) required channels to grid
        for key, limit in limits.items():
            if limit == 0:
                continue

            # TODO: do it in batches
            data = numpy.load(os.path.join(results_dir, '{}_set.npy'.format(key)))

            grided_x = convert_to_grid(data[..., in_ch], in_edges)
            if in_buckets > 0:
                grided_x = grided_x / in_buckets  # TODO: verify that it normalizes correctly

            grided_y = convert_to_grid(data[..., out_ch], out_edges)
            if out_buckets > 0:
                grided_y = grided_y / out_buckets

            combined = numpy.dstack((grided_x, grided_y))

            out_path = os.path.join(results_dir, 'grided_{}_set.npy'.format(key))

            numpy.save(out_path, combined)
            logger.info('{} saved.'.format(out_path))


def add_subset_parser(subparsers=None):
    description = 'create random subset of full dataset'

    if subparsers is not None:
        parser = subparsers.add_parser('subset', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
    parser.add_argument(
        '-l',
        '--train-batches',
        type=int,
        default=32
    )
    parser.add_argument(
        '-a',
        '--validation-batches',
        type=int,
        default=8
    )
    parser.add_argument(
        '-t',
        '--test-batches',
        type=int,
        default=10
    )

    parser.add_argument(
        '-g',
        '--grided',
        action='store_true',
        help='Use GridedDataSequence instead of basic DataSequence'
    )
