from analysta.cli.arguments.types import readable_file, writable_dir


def add_config_argument(parser, required=True):
    parser.add_argument(
        '-c',
        '--config',
        metavar='CONFIG_FILE.JSON',
        type=readable_file,
        required=required
    )


def add_report_argument(parser, required=True):
    parser.add_argument(
        '-r',
        '--report',
        metavar='REPORT_FILE.CSV',
        type=readable_file,
        required=required
    )


def add_limit_argument(parser, help_text='data batches limit; defaults to all', metavar='BATCHES_LIMIT'):
    parser.add_argument(
        '-l',
        '--limit',
        type=int,
        metavar=metavar,
        default=None,
        help=help_text
    )


def add_results_argument(parser):
    parser.add_argument(
        '-d',
        '--results-dir',
        metavar='DIR',
        type=writable_dir,
        default='results',
        help='directory to store results in'
    )
