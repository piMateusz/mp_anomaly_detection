import argparse
import logging

from analysta.cli.arguments.common import add_config_argument, add_results_argument, add_limit_argument
from analysta.detector.detector import Detector
from analysta.utils.config.get_config_instance import get_config_instance
from analysta.utils.results.saving import save_all


def add_single_parser(subparsers=None):
    description='run model with setup specified in CONFIG_FILE.JSON'

    if subparsers is not None:
        parser = subparsers.add_parser('single', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
    add_limit_argument(parser)


def run_single(config, setup_writer=None, results_dir=None, limit=None, **kwargs):
    """
    
    :param results_dir: Directory to store generated files in
    :param config: Config file path or config object
    :param setup_writer: InfrequentWriter
    :return:
    """
    logger = logging.getLogger(__name__)

    config_instance = get_config_instance(config)

    logger.info("Detector run for config: {}".format(config))

    detector = Detector(config_instance)

    detector.run(limit)

    config_instance, setup_writer = save_all(setup_writer, config_instance, detector, logger, results_dir=results_dir)

    return config_instance, setup_writer, detector


