# -*- coding: utf-8 -*-
import argparse
import logging
import pprint

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.detector import Detector
from analysta.utils.config.get_config_instance import get_config_instance
from analysta.utils.results import save_all


def read_param_array(config, key, default_value=None):
    try:
        param_array = config[key]
    except KeyError:
        param_array = []
    param_array = param_array if len(param_array) else [default_value]
    return param_array


def add_sweep_parser(subparsers=None):
    description = 'run sweep for model defined in CONFIG_FILE.JSON'

    if subparsers is not None:
        parser = subparsers.add_parser('sweep', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)

    add_results_argument(parser)

    parser.add_argument(
        '-m',
        '--mute-detector',
        action='store_true'
    )


def run_sweep(config, results_dir, mute_detector=False, **kwargs):
    # TODO: switch to similar config as optimizer uses instead of old arrays-in-base-config format
    # TODO: add mute detector option
    logger = logging.getLogger(__name__)

    if mute_detector:
        effective_level = logger.getEffectiveLevel()

        logging.getLogger('analysta').setLevel(logging.WARNING)  # silence all of analysta
        logger.setLevel(effective_level)  # enable self logging

    logger.info('Sweep starting...')

    base_config = get_config_instance(config)
    writer = None

    # get configuration
    instances = base_config.get('setup.instances', 1)

    look_back_array = base_config['preparation.look_back']
    look_ahead_array = read_param_array(base_config, 'preparation.look_ahead', 1)
    in_grid_array = read_param_array(base_config, 'preparation.in_buckets', 0)
    out_grid_array = read_param_array(base_config, 'preparation.out_buckets')
    in_algorithm_array = read_param_array(base_config, 'preparation.in_bucketization', "none")
    out_algorithm_array = read_param_array(base_config, 'preparation.out_bucketization')
    samples_percentage_array = read_param_array(base_config, 'preparation.samples_percentage', 1)

    batch_size_array = base_config['preparation.batch_size']
    cells_array = base_config['model.cells']
    max_nb_epoch_array = base_config['model.max_nb_epoch']

    rules_array = read_param_array(base_config, 'analyzer.rules')

    edges = None
    model = None

    for in_grid in in_grid_array:
        for out_grid in out_grid_array:
            if out_grid is None:
                out_grid = in_grid

            for in_algorithm in in_algorithm_array:
                for out_algorithm in out_algorithm_array:
                    if out_algorithm is None:
                        out_algorithm = in_algorithm

                    for samples_percentage in samples_percentage_array:
                        for look_back in look_back_array:
                            for look_ahead in look_ahead_array:

                                for cells in cells_array:
                                    if isinstance(cells, int):
                                        cells = (cells,)
                                    else:
                                        cells = tuple(cells)
                                    for batch_size in batch_size_array:
                                        # TODO: can you change batch size during a single model lifetime too?
                                        #     or (as now) does it imply new model is needed?

                                        for _ in range(instances):
                                            # how many instances of each model to create?

                                            for max_nb_epoch in max_nb_epoch_array:
                                                if model is not None:
                                                    model.max_epochs = max_nb_epoch

                                                for rules in rules_array:
                                                    single_config = base_config.copy({
                                                        'preparation.look_back': look_back,
                                                        'preparation.look_ahead': look_ahead,
                                                        'preparation.in_buckets': in_grid,
                                                        'preparation.out_buckets': out_grid,
                                                        'preparation.in_bucketization': in_algorithm,
                                                        'preparation.out_bucketization': out_algorithm,
                                                        'preparation.samples_percentage': samples_percentage,
                                                        'model.cells': cells,
                                                        'preparation.batch_size': batch_size,
                                                        'model.max_nb_epoch': max_nb_epoch,
                                                        'analyzer.rules': rules
                                                    })

                                                    detector = Detector(single_config)

                                                    # try to reuse existing instances
                                                    if edges is not None:
                                                        detector.edges = edges
                                                    if model is not None:
                                                        detector.model = model

                                                    try:
                                                        logger.info("Running sweep for preparation:\n{}\nmodel:\n{}".format(
                                                            pprint.pformat(single_config['preparation'], indent=4),
                                                            pprint.pformat(single_config['model'], indent=4)
                                                        ))
                                                        detector.run()
                                                        single_config, writer = save_all(writer, single_config,
                                                                                         detector, None, results_dir)
                                                        logger.info("Results:\n{}".format(pprint.pformat(
                                                            single_config['out.model.test'],
                                                            indent=4
                                                        )))

                                                        # remember the data for future re-use
                                                        edges = detector.edges
                                                        model = detector.model
                                                    except MemoryError as error:
                                                        logger.critical(error)
                                                        continue

                                            # at this point model structure changes
                                            getattr(model, 'cleanup', lambda: False)()
                                            del model
                                            model = None

                    # at this point edges_path changes
                    del edges
                    edges = None

    logger.info('Sweep done')

