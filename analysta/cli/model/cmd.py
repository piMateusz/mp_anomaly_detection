import logging

from analysta.cli.model.run_analyzer import add_analyzer_parser
from analysta.cli.model.run_optimize import add_optimize_parser
from analysta.cli.model.run_single import add_single_parser
from analysta.cli.model.run_sweep import add_sweep_parser
from analysta.utils import get_definition_from_path


def add_parser_args(parser):
    subparsers = parser.add_subparsers(dest='mode')

    add_single_parser(subparsers)
    add_analyzer_parser(subparsers)
    add_optimize_parser(subparsers)
    add_sweep_parser(subparsers)


def main(args):
    try:
        run_func = get_definition_from_path(
            'analysta.cli.model.run_{}.run_{}'.format(args.mode, args.mode),
            'run function'
        )
        run_func(**vars(args))
    except ModuleNotFoundError:
        logging.getLogger(__name__).critical('Unknown "model" mode')
        raise
