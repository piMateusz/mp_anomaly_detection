import argparse

from analysta.cli.arguments.common import add_config_argument, add_results_argument, add_report_argument
from analysta.cli.model.run_single import run_single
from analysta.utils.config import Config
from analysta.utils.results import ResultsReader


def add_analyzer_parser(subparsers=None):
    description = 'run analyzer on test data configured according to CONFIG_FILE.JSON for each model configuration ' \
                  'from SETUP_FILE.JSON'

    if subparsers is not None:
        parser = subparsers.add_parser('analyzer', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    config_or_setup = parser.add_mutually_exclusive_group(required=True)
    add_config_argument(config_or_setup, required=False)
    add_report_argument(config_or_setup, required=False)

    add_results_argument(parser)

    parser.add_argument(
        '-f',
        '--find-rules',
        action='store_true',
        help='Force finding rules, even if they are specified in setup/config'
    )

    parser.add_argument(
        '-t',
        '--test-paths',
        help='Override test paths',
        default=None
    )


def run_analyzer(report=None, config=None, results_dir=None, find_rules=False, test_paths=None, **kwargs):
    configurations = []

    if report is not None:
        results_reader = ResultsReader(report)
        config_instance = Config()
        for setup_instance in results_reader:
            configurations.append(config_instance.copy(setup_instance))

    if config is not None:
        configurations.append(Config(config))

    results_writer = None

    for config_instance in configurations:
        if test_paths is not None:
            config_instance['data.test_paths'] = test_paths

        if find_rules:
            try:
                del config_instance['analyzer.rules']
            except KeyError:
                pass

        config_instance['analyzer.disabled'] = False
        config_instance['out.results_path'] = None

        config_instance, results_writer, _ = run_single(config_instance, results_writer, results_dir)
