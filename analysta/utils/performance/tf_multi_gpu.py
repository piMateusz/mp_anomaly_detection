try:
    from keras.backend import backend

    if backend() != 'tensorflow':
        raise ImportError

    import tensorflow as tf
    from tensorflow.python.client import device_lib

    def get_available_gpus():
        local_device_protos = device_lib.list_local_devices()
        return sum([1 for x in local_device_protos if x.device_type == 'GPU'])

    if get_available_gpus() > 1:
        from keras.utils.training_utils import multi_gpu_model
    else:
        raise ImportError
except ImportError:
    class PassTroughWith(object):
        def __enter__(self):
            pass

        def __exit__(self, *args):
            return False

    class DummyTF(object):
        def device(self, name):
            return PassTroughWith()

    def multi_gpu_model(x, *args, **kwargs):
        return x

    def get_available_gpus():
        return 0

    tf = DummyTF()
