import collections
import copy
import json
import os
from collections import OrderedDict

import six


class Config(collections.MutableMapping):
    def __iter__(self):
        for group, items in self.__data.items():
            for key, value in sorted(items.items(), key=lambda t: t[0]):
                yield '.'.join([group, key])

    def __len__(self):
        return len(self.flatten())

    def __delitem__(self, name):
        name = name.split('.')

        val = self.__data
        for key in name[:-1]:
            val = val[key]

        del val[name[-1]]

    def __init__(self, config_path=None, root=None):
        self.__path = config_path
        if config_path is not None:
            self.__root = os.path.dirname(self.__path)
            with open(config_path) as fh:
                self.__data = json.load(fh)
        else:
            self.__root = os.getcwd() if root is None else root
            self.__data = {}
        self.__cache = {}

    def __getitem__(self, name):
        is_path = ('load' in name or 'path' in name) and 'prefix' not in name
        is_channel = 'channels' in name
        name = name.split('.')

        val = self.__data
        up_val = None
        for key in name:
            up_val = val
            val = val[key]

        if val and is_path:
            try:
                prefix = up_val[key + '_prefix']
            except KeyError:
                prefix = ''
            if isinstance(val, list):
                val = self.__convert_paths_to_absolute(val, prefix)
            elif isinstance(val, dict):
                val = OrderedDict(zip(val.keys(), self.__convert_paths_to_absolute(val.values(), prefix)))
            else:
                val = self.__convert_paths_to_absolute([val])[0]
        elif val and is_channel:
            val = self.__convert_tuples_to_ranges(val)

        return val

    def __setitem__(self, name, value):
        if name in self.__cache.keys():
            del self.__cache[name]

        name = name.split('.')

        val = self.__data
        for key in name[:-1]:
            try:
                val = val[key]
            except KeyError:
                val[key] = {}
                val = val[key]

        val[name[-1]] = value

    def __contains__(self, name):
        name = name.split('.')

        val = self.__data
        for key in name:
            try:
                val = val[key]
            except KeyError:
                return False

        return True

    def copy(self, updates=None):
        new = copy.deepcopy(self)

        if updates:
            for key, value in updates.items():
                try:
                    new[key].update(value)
                except (KeyError, AttributeError):
                    new[key] = value

        return new

    def __str__(self):
        import pprint
        return pprint.pformat(self.__data)

    def flatten(self):
        def add_to_flat(flat, data, group=None, sort=True):
            sorted_items = sorted(data.items(), key=lambda t: t[0]) if sort else data.items()
            for key, value in sorted_items:
                flat_key = key if group is None else group + '.' + key
                if not isinstance(value, dict):
                    flat[flat_key] = value
                else:
                    add_to_flat(flat, value, flat_key)
            return flat

        return add_to_flat(collections.OrderedDict(), self.__data, sort=False)

    def __convert_paths_to_absolute(self, values, prefix=''):
        paths = []
        for v in values:
            if isinstance(v, six.string_types):
                paths.append(os.path.realpath(os.path.join(self.__root, prefix, v)))
            else:
                paths.append(self.__convert_paths_to_absolute(v, prefix))
        return paths

    @staticmethod
    def __convert_tuples_to_ranges(value):
        new_value = []
        for v in value:
            if isinstance(v, list):
                new_value += list(range(*v))
            else:
                new_value.append(v)
        return new_value

    def load_paths(self, key):
        paths = self[key] if key in self else []

        if not isinstance(paths, (list, dict)):
            # paths can be stored in an external files
            if key not in self.__cache.keys():
                paths_prefix = self[key + '_prefix'] if key + '_prefix' in self else ''
                with open(paths) as fp:
                    paths = self.__convert_paths_to_absolute([name.strip() for name in fp.readlines()], paths_prefix)
            else:
                paths = self.__cache[key]

        if isinstance(paths, list):
            paths = OrderedDict(zip([
                os.path.splitext(os.path.basename(p))[0] for p in paths
            ], paths))

        return paths

    @property
    def root(self):
        return self.__root

    @property
    def path(self):
        return self.__path

    def dump(self, save_path):
        with open(save_path, 'w') as fh:
            json.dump(self.__data, fh, indent=4)
