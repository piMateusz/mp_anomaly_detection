import importlib
import six


def get_definition_from_path(path, desc):
    if not isinstance(path, six.string_types):
        return path

    pkg_index = path.rfind('.')

    if pkg_index == -1:
        raise ValueError('Invalid %s class path. Please specify the module path.' % (desc,))

    cls = path[pkg_index + 1:]
    pkg = path[:pkg_index]

    imported_module = importlib.import_module(pkg)
    return getattr(imported_module, cls)
