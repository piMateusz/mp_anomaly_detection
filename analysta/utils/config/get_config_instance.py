from analysta.utils.config import Config


def get_config_instance(config):
    config_instance = config if isinstance(config, Config) else Config(config)
    return config_instance
