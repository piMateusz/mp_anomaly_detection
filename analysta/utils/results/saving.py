import logging

import numpy
import os

from .infrequent_writer import InfrequentWriter
from .results_path import get_results_path


def save_report(config_instance, logger, results_writer=None, results_dir=None):
    logger.info('Saving report...')
    report_path = get_results_path('report', subdir_name=results_dir)
    if not results_writer:
        results_writer = InfrequentWriter(
            report_path,
            config_instance.flatten().keys())
    results_writer.write(config_instance.flatten())
    logger.info('Report saved')
    return results_writer


def save_results(config_instance, results, logger, results_dir=None):
    if results is not None and config_instance.get('model.save_results', True):
        logger.info('Saving model results...')
        detector_results_path = get_results_path('detector_results', 'npz', results_dir)
        numpy.savez_compressed(
            detector_results_path,
            **results
        )
        config_instance['out.results_path'] = os.path.relpath(detector_results_path, config_instance.root)
        logger.info('Results saved')
        return config_instance['out.results_path']
    else:
        return None


def save_model(config_instance, detector, logger, results_dir=None):
    model_path = config_instance.get('model.save', True)
    if model_path and isinstance(model_path, bool):
        model_path = get_results_path('model', 'h5', results_dir)

    logger.info('Saving model...')
    saved = detector.model.save(model_path)
    if saved:
        config_instance['model.load'] = os.path.relpath(model_path, config_instance.root)
        logger.info('Model saved')
        return config_instance['model.load']
    else:
        return None


def save_edges(config_instance, detector, logger, results_dir=None):
    edges_path = get_results_path('edges', 'npz', results_dir)
    logger.info('Saving edges...')
    numpy.savez(
        edges_path,
        **detector.edges
    )
    config_instance['preparation.edges_path'] = os.path.relpath(edges_path, config_instance.root)
    logger.info('Edges saved')
    return config_instance['preparation.edges_path']


def save_curve(curve_type, config_instance, detector, logger, results_dir=None):
    curve_data = detector.stats.get('out.model.test.{}_curve'.format(curve_type), None)

    if curve_data is None:
        return None

    curve_path = get_results_path('{}_curve'.format(curve_type), 'npz', results_dir)
    logger.info('Saving {} curve...'.format(curve_type.upper()))
    numpy.savez(
        curve_path,
        **curve_data
    )
    config_instance['out.model.test.{}_curve_path'.format(curve_type)] = curve_path
    logger.info('{} curve saved'.format(curve_type.upper()))
    return config_instance['out.model.test.{}_curve_path'.format(curve_type)]


def save_all(report_writer, config_instance, detector, logger=None, results_dir=None):
    if logger is None:
        logger = logging.getLogger(__name__)

    # remember stats
    config_instance = extract_stats_from_detector(config_instance, detector)

    save_curve('pr', config_instance, detector, logger, results_dir)
    save_curve('roc', config_instance, detector, logger, results_dir)

    if 'preparation.edges_path' not in config_instance:
        save_edges(config_instance, detector, logger, results_dir)

    if not config_instance.get('preparation.edges_path'):
        save_edges(config_instance, detector, logger, results_dir)

    if config_instance.get('model.save', True) and (
        not config_instance.get('model.load', False)
        or
        config_instance.get('model.retraining', False)
    ):
        save_model(config_instance, detector, logger, results_dir)

    report_writer = save_report(config_instance, logger, report_writer, results_dir)

    return config_instance, report_writer


def extract_stats_from_detector(config_instance, detector):
    exclude = [
        'out.model.test.pr_curve',
        'out.model.test.roc_curve'
    ]

    for key, val in detector.stats.items():
        if key not in exclude:
            config_instance[key] = val

    # TODO: add execution time column(s) - fit, train predict,
    #     test predict, data prep, ddi calc, rules finding...

    return config_instance
