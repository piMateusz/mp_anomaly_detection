import ast
import csv
import os

import numpy


class ResultsReader(object):
    def __init__(self, setup_path):
        self.__root = os.path.dirname(setup_path)

        # temporary, until switch to new format is done
        self.__setup_format = None

        self.__data = []
        with open(setup_path) as sf:
            reader = csv.DictReader(sf)
            for row in reader:
                self.__data.append(self.__convert_row(row))

        self.__last_setup = None

    def __convert_row(self, row):
        detector_results_path = self.__extract(row, 'out.results_path', self.get_path)
        edges_path = self.__extract(row, 'preparation.edges_path', self.get_path)
        model_path = self.__extract(row, 'model.load', self.get_path)

        cells = self.__extract(row, 'model.cells', ast.literal_eval)

        train_paths = row.get('data.train_paths', None)
        test_paths = row.get('data.test_paths', None)
        val_paths = row.get('data.val_paths', None)

        try:
            train_paths = ast.literal_eval(train_paths)
        except ValueError:
            pass

        try:
            test_paths = ast.literal_eval(test_paths)
        except ValueError:
            pass
            
        try:
            val_paths = ast.literal_eval(val_paths)
        except ValueError:
            pass

        to_return = {
            'setup': {
                'model': row.get('setup.model'),
                'model': row.get('setup.model'),
            },
            'data': {
                'input_channels': ast.literal_eval(row.get('data.input_channels')) if row.get(
                    'data.input_channels') else None,
                'output_channels': ast.literal_eval(row.get('data.output_channels')) if row.get(
                    'data.output_channels') else None,
                'channels_names': self.__extract(row, 'data.channels_names', ast.literal_eval),
                'train_paths': train_paths,
                'test_paths': test_paths,
                'val_paths': val_paths,
                'train_paths_prefix': row.get('data.train_paths_prefix', None),
                'test_paths_prefix': row.get('data.test_paths_prefix', None),
                'val_paths_prefix': row.get('data.val_paths_prefix', None),
            },
            'preparation': {
                'look_back': int(row['preparation.look_back']),
                'look_ahead': int(row['preparation.look_ahead']),
                'samples_percentage': float(row['preparation.samples_percentage']),
                'in_buckets': int(row['preparation.in_buckets']),
                'out_buckets': int(
                    row['preparation.out_buckets'] if row['preparation.out_buckets'] else row['preparation.in_buckets']),
                'in_bucketization': row['preparation.in_bucketization'],
                'out_bucketization': row['preparation.out_bucketization'] if row['preparation.out_bucketization'] else row[
                    'preparation.in_bucketization'],
                'edges_path': edges_path
            },
            'model': {
                'cells': cells,
                'batch_size': int(row['preparation.batch_size']),
                'max_nb_epoch': int(row.get('model.max_nb_epoch', row['model.max_nb_epoch'])),
                'early_stopping': bool(row.get('model.early_stopping', False)),
                'load': model_path
            },
            'analyzer': {
                'rules': ast.literal_eval(row.get('analyzer.rules'))
                if row.get('analyzer.rules')
                else None
            },
            'out': {
                'model': {
                    'train': {
                        'acc': self.__extract(row, 'out.model.train.acc'),
                        'nb_epoch': self.__extract(row, 'out.model.train.nb_epoch', int),
                    },
                    'val': {
                        'acc': self.__extract(row, 'out.model.val.acc'),
                    },
                    'test': {
                        'acc': self.__extract(row, 'out.model.test.acc'),
                        'precision': self.__extract(row, 'out.model.test.precision'),
                        'recall': self.__extract(row, 'out.model.test.recall'),
                        'f1score': self.__extract(row, 'out.model.test.f1score'),
                        'f2score': self.__extract(row, 'out.model.test.f2score'),
                        'confusion_matrix': row.get('out.model.test.confusion_matrix'),
                        'pr_curve_path': self.__extract(row, 'out.model.test.pr_curve_path', self.get_path),
                        'roc_curve_path': self.__extract(row, 'out.model.test.roc_curve_path', self.get_path),
                        'average_precision_score': self.__extract(row, 'out.model.test.average_precision_score'),
                    }
                },
                'results_path': detector_results_path,
            }
        }

        return to_return

    def get_path(self, value):
        return os.path.join(self.__root, os.path.basename(value))

    def __iter__(self):
        for setup in self.__data:
            self.__last_setup = setup
            yield setup

    @staticmethod
    def __extract(row, key, as_type=float, default=None):
        return as_type(row[key]) if key in row and row[key] is not None and row[key] != '' else default

    def load_last_edges(self):
        return numpy.load(self.__last_setup['preparation']['edges_path'])

    def load_last_results(self):
        return numpy.load(self.__last_setup['out']['results_path'])
