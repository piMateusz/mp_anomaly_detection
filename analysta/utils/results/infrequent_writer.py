import csv
import sys
import os


class InfrequentWriter(object):
    def __init__(self, file_path, fieldnames):
        self.__file_path = file_path
        self.__fieldnames = fieldnames
        self.__binary_flag = '' if sys.version_info.major > 2 else 'b'

        if os.path.exists(self.__file_path):
            with open(self.__file_path, 'r' + self.__binary_flag) as csv_file:
                reader = csv.DictReader(csv_file)
                if set(reader.fieldnames) != set(self.__fieldnames):
                    raise ValueError('Setup file exists, but fieldnames do not match. Refusing to write.')
        else:
            with open(self.__file_path, 'w' + self.__binary_flag) as csv_file:
                writer = csv.DictWriter(csv_file,
                                        fieldnames=self.__fieldnames)
                writer.writeheader()

    def write(self, data_dict):
        with open(self.__file_path, 'a' + self.__binary_flag) as csv_file:
            writer = csv.DictWriter(csv_file,
                                    fieldnames=self.__fieldnames)
            writer.writerow(data_dict)

