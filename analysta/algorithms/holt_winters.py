class HoltWinters(object):
    """
    1. Holt-Winters model with the anomalies detection using Brutlag method
    2. Non-seasonal series (e.g. stock prices) cannot be forecasted using this method
    3. We need to compute the average level for every observed season we have, divide every observed value by
        the average for the season it's in and finally average each of these numbers across our observed seasons.
    """

    def __init__(self, alpha, beta, gamma, season_length=24):
        """
        :param alpha: Holt-Winters model smoothing coefficient
        :param beta: Holt-Winters model smoothing coefficient
        :param gamma: Holt-Winters model smoothing coefficient
        :param season_length: length of a season
        """
        self.season_length = season_length
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

    def initial_trend(self, series):
        """
        compute the average of trend averages across seasons.
        :return:
        """
        sum = 0.0
        for i in range(self.season_length):
            sum += float(series[i + self.season_length] - series[i]) / self.season_length
        return sum / self.season_length

    def initial_seasonal_components(self, series):
        """
        we need to compute the average level for every observed season we have, divide every observed value by
        the average for the season it's in and finally average each of these numbers across our observed seasons.
        :return:
        """
        seasonals = {}
        season_averages = []
        n_seasons = int(len(series) / self.season_length)
        # let's calculate season averages
        for j in range(n_seasons):
            season_averages.append(sum(series[self.season_length * j:self.season_length * j + self.season_length]) / float(self.season_length))
        # let's calculate initial values
        for i in range(self.season_length):
            sum_of_vals_over_avg = 0.0
            for j in range(n_seasons):
                sum_of_vals_over_avg += series[self.season_length * j + i] - season_averages[j]
            seasonals[i] = sum_of_vals_over_avg / n_seasons
        return seasonals

    def predict(self, series):
        result = []
        components = self.initial_seasonal_components(series)
        smooth = series[0]
        trend = self.initial_trend(series)
        result.append(series[0])

        for i in range(1, len(series) + 1):
            if i >= len(series):  # we are forecasting
                m = i - len(series) + 1
                result.append((smooth + m * trend) + components[i % self.season_length])
            else:
                val = series[i]
                last_smooth, smooth = smooth, self.alpha * (val - components[i % self.season_length]) \
                                      + (1 - self.alpha) * (smooth + trend)
                trend = self.beta * (smooth - last_smooth) + (1 - self.beta) * trend
                components[i % self.season_length] = self.gamma * (val - smooth) + (1 - self.gamma) * components[i % self.season_length]
                result.append(smooth + trend + components[i % self.season_length])
        return result
