import numpy as np


def partial_autocorrelation(data, shift):
    """

    :param data:
    :param shift:
    :return:
    """
    pac = list()
    residuals = data

    for i in range(1, shift):
        data_shifted = data[:-i]
        residuals_shifted = residuals[i:]
        print("data_shifted: {}, residuals_shifted: {}".format(data_shifted.shape, residuals_shifted.shape))
        print("data_shifted: {}, residuals_shifted: {}".format(data_shifted
                                                               , residuals_shifted))

        correlation = np.corrcoef(
            data[:-i], residuals[i:])[0, 1]  # take right top value from the symmetric matrix
        pac.append(correlation)

        # fit new shifted data to the residuals (y = a*x + b)
        [a, b] = np.polyfit(data[:-i], residuals[i:], 1)
        estimate = b + a * data[:-i]
        # update residuals
        residuals[i:] = residuals[i:] - estimate

    return pac
