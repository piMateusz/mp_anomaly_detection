import math

from torch import nn as nn


class Interpolate(nn.Module):
    def __init__(self, **kwargs):
        super(Interpolate, self).__init__()
        self.kwargs = kwargs

    def forward(self, x):
        return nn.functional.interpolate(x, **self.kwargs)


class Generator(nn.Module):
    def __init__(self, nz, target_shape):
        super(Generator, self).__init__()

        self.__input_width = int(math.ceil(target_shape[0] / 4))
        self.__input_height = target_shape[1]

        self.__target_depth = 1
        self.__target_width = target_shape[0]
        self.__target_height = target_shape[1]
        self.__output_padding = (self.__target_width - self.__input_width * 2, 0)

        self.__nz = nz

        # layers definition
        self.l1 = nn.Linear(self.__input_height, int(self.__input_height / 2))
        self.batch_norm_0 = nn.BatchNorm2d(self.__nz)
        self.upsample_1 = Interpolate(scale_factor=2)
        self.conv_2 = nn.Conv2d(self.__nz, 128, 3, stride=1, padding=1)
        self.batch_norm_3 = nn.BatchNorm2d(128, 0.8)
        self.leakyrelu_4 = nn.LeakyReLU(0.2, inplace=True)
        # self.upsample_5 = Interpolate(scale_factor=2)
        # self.conv_6 = nn.Conv2d(128, 64, 3, stride=1, padding=1)
        self.conv_6 = nn.ConvTranspose2d(128, 64, 3, stride=1,
                                         padding=2, dilation=2, output_padding=self.__output_padding)
        self.batch_norm_7 = nn.BatchNorm2d(64, 0.8)
        self.leakyrelu_8 = nn.LeakyReLU(0.2, inplace=True)
        self.conv_9 = nn.Conv2d(64, self.__target_depth, 3, stride=1, padding=1)
        self.tanh_10 = nn.Tanh()

        # self.conv_blocks = nn.Sequential(
        #     nn.BatchNorm2d(128),
        #     nn.Upsample(scale_factor=2),
        #     nn.Conv2d(128, 128, 3, stride=1, padding=1),
        #     nn.BatchNorm2d(128, 0.8),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     nn.Upsample(scale_factor=2),
        #     nn.Conv2d(128, 64, 3, stride=1, padding=1),
        #     nn.BatchNorm2d(64, 0.8),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     nn.Conv2d(64, number_of_channels, 3, stride=1, padding=1),
        #     nn.Tanh()
        # )

    def forward(self, z):
        out = self.l1(z)
        # print('shape 0: ', out.shape)
        # out = out.view(out.shape[0], 128, self.init_size, self.init_size)
        out = self.batch_norm_0(out)
        # print('shape 1: ', out.shape)
        out = self.upsample_1(out)
        # print('shape 2: ', out.shape)
        out = self.conv_2(out)
        # print('shape 3: ', out.shape)
        out = self.batch_norm_3(out)
        # print('shape 4: ', out.shape)
        out = self.leakyrelu_4(out)
        # print('shape 5: ', out.shape)
        # out = self.upsample_5(out)
        # print('shape 6: ', out.shape)
        out = self.conv_6(out)
        # print('shape 7: ', out.shape)
        out = self.batch_norm_7(out)
        # print('shape 8: ', out.shape)
        out = self.leakyrelu_8(out)
        # print('shape 9: ', out.shape)
        out = self.conv_9(out)
        # print('shape 10: ', out.shape)
        out = self.tanh_10(out)

        # print('out after reshape shape: ', out.shape)
        # img = self.conv_blocks(out)
        # print('img shape: ', img.shape)
        return out

    @property
    def input_width(self):
        return self.__input_width

    @property
    def input_height(self):
        return self.__input_height
