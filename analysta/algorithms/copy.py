import numpy


class Copy(object):
    """
        Copy the last value
    """

    def predict(self, series):
        return numpy.concatenate(([series[0]], series))
