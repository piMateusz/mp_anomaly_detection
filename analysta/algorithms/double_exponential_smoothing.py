import numpy as np
from scipy import signal


class DoubleExponentialSmoothing(object):
    """
    Double exponential smoothing then is nothing more than
    exponential smoothing applied to both level and trend.
        series - time series
        alpha - float [0.0, 1.0], smoothing parameter for level
        beta - float [0.0, 1.0], smoothing parameter for trend
    """

    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def predict_manual(self, series):
        result = [series[0], series[1]]

        for n in range(1, len(series)):
            result.append(
                series[n] * (1 + self.beta) * self.alpha
                - series[n-1] * self.alpha
                + result[n] * (2 - self.alpha - self.alpha * self.beta)
                - result[n-1] * (1 - self.alpha)
            )

        return np.array(result)

    def predict(self, series):
        try:
            b = np.array([
                (1 + self.beta) * self.alpha,
                -self.alpha
            ])
            a = np.array([
                1,
                -2 + self.alpha + self.alpha * self.beta,
                1 - self.alpha
            ])

            zi = np.array([
                1.0 - self.alpha - self.alpha * self.beta,
                self.alpha - 1.0
            ])  # same as zi = signal.lfilter_zi(b, a)
            zi_shape = list(series.shape)
            zi_shape[0] = -1
            result, _ = signal.lfilter(b, a, series, zi=(zi*series[0]).reshape(*zi_shape), axis=0)

            return np.concatenate(([series[0]], result))
        except np.linalg.linalg.LinAlgError:
            return self.predict_manual(series)
