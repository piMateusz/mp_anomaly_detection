import numpy as np
import torch
import torch.cuda

from analysta.algorithms.gan import GAN
from analysta.algorithms.gan_rnn.discriminator import Discriminator
from analysta.algorithms.gan_rnn.generator import Generator


class GANRNN(GAN):

    def __init__(self,
                 max_nb_epoch,
                 x_shape,
                 gpus,
                 generator_class=Generator,
                 discriminator_class=Discriminator):
        super().__init__(max_nb_epoch, x_shape, gpus,
                         generator_class=generator_class,
                         discriminator_class=discriminator_class)

    def data_to_torch(self, data):
        if isinstance(data, np.ndarray):
            data_torch = torch.from_numpy(data).float()
        else:
            data_torch = data
        return data_torch.to(self.at.device)

    def generate_batch_of_random_vectors(self, batch_size):
        # Generate batch of latent vectors
        noise = torch.randn(
            batch_size,
            self.input_width,
            self.input_height,
            device=self.at.device
        )
        return noise
