import math

import torch
from torch import nn as nn


class Generator(nn.Module):
    def __init__(self, nz, target_shape):
        super(Generator, self).__init__()

        self.__input_width = target_shape[0]  # look_back
        self.__input_height = target_shape[1]  # input_size

        self.__target_width = target_shape[1]
        self.__target_height = target_shape[0]

        # RNN definition
        input_size = self.__input_height
        self.hidden_size = 128
        self.num_layers = 1

        # linear layer definition
        out_linear_size = self.__target_height  * self.__target_width

        # LSTM model definition
        self.rnn = nn.LSTM(input_size=input_size,
                           hidden_size=self.hidden_size,
                           num_layers=self.num_layers,
                           batch_first=True)

        self.linear = nn.Linear(in_features=self.hidden_size, out_features=out_linear_size)

    def forward(self, z):
        cuda_check = z.is_cuda
        if cuda_check:
            get_cuda_device = z.get_device()
        batch_size = z.shape[0]
        h0_zero = torch.zeros(self.num_layers, batch_size, self.hidden_size, device=get_cuda_device)
        c0_zero = torch.zeros(self.num_layers, batch_size, self.hidden_size, device=get_cuda_device)

        output_data, _ = self.rnn(z, (h0_zero, c0_zero))
        out_linear = self.linear(output_data)[:, -1, :]
        out = out_linear.view(-1, self.__target_width, self.__target_height)
        return out

    @property
    def input_width(self):
        return self.__input_width

    @property
    def input_height(self):
        return self.__input_height
