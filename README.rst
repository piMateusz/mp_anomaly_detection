=======
Analysta - data analysis and anomaly detection
=======

----

What is this?
=============

A framework initially developed for conducting the experiments with CERN superconducting magnets data.
Here are some links to publications describing the results (reverse chronological order):

* `Protection of Superconducting Industrial Machinery Using RNN-Based Anomaly Detection for Implementation in Smart Sensor <https://doi.org/10.3390/s18113933>`_
* `The model of an anomaly detector for HiLumi LHC magnets based on Recurrent Neural Networks and adaptive quantization <https://doi.org/10.1016/j.engappai.2018.06.012>`_
* `Using LSTM recurrent neural networks for monitoring the LHC superconducting magnets <https://doi.org/10.1016/j.nima.2017.06.020>`_

----

Docs
====

For documentation please consult the `Wiki <https://bitbucket.org/maciekwielgosz/anomaly_detection/wiki/Home>`_.

Examples (in the form of Jupyter Notebooks) can be found in `examples <https://bitbucket.org/maciekwielgosz/anomaly_detection/src/9f803ea868c9/examples/>`_. Currently available are:

* `Basic flow of using 'analysta' to work with custom data <https://bitbucket.org/maciekwielgosz/anomaly_detection/src/9f803ea868c9/examples/beijing_PM25/BeijingPM2.5.ipynb?viewer=nbviewer>`_ (using `Beijing PM2.5 dataset <https://archive.ics.uci.edu/ml/datasets/Beijing+PM2.5+Data>`_).
* `Hydraulic Systems data <https://bitbucket.org/maciekwielgosz/anomaly_detection/src/9f803ea868c9/examples/hydraulic_systems/HydraulicSystems.ipynb?viewer=nbviewer>`_ (using `Beijing PM2.5 dataset <https://archive.ics.uci.edu/ml/datasets/Beijing+PM2.5+Data>`_).

----

Note
====

This project has been set up using PyScaffold 2.5.7. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.

----

License
=======

MIT License